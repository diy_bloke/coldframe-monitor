//Tx=1
//Rx=3
//D0=16 
//D1=5=SCL
//D2=4=SDA
//D3=0^
//D4=2^  Sinkfan
//D5=14 -> Motor
//D6=12 ->Fan
//D7=13 ->OneWireBus
//D8=15 ->
//tcs34725


#define SINK_FAN D4
#define MOTOR_PIN D5 //14 
#define FAN_PIN D6  //12
#define ONE_WIRE_BUS 13// D7 
byte klepPin = 16; //D0
byte KlepOpen=15;
boolean Open;

//----- voor DS18B20
#include <OneWire.h> //http://www.pjrc.com/teensy/td_libs_OneWire.html
#include <DallasTemperature.h> //  http://milesburton.com/Main_Page?title=Dallas_Temperature_Control_Library

//------ voor Telegram BOT
#include <UniversalTelegramBot.h>
#define BOTtoken "977142506:AAHSWypUbNN8BCthOK-Fg674E3r2yCsGk"  // your Bot Token (Get from Botfather)
String chat_id="645672785";
WiFiClientSecure botclient;
UniversalTelegramBot bot(BOTtoken, botclient);
bool midnightFlag=0;
bool warningFlag=1;
bool OnChipHeatFlag=0;
String midnightString;
float maxtemp=-40.0;
float mintemp=40.0;
String maxtime;
String mintime;
//-------


//For BH1750
#include <Wire.h>
#include <BH1750.h>
BH1750 lightMeter;
//---------------
#include "SparkFunHTU21D.h"
//Create an instance of the object
HTU21D myHumidity;
int HTU21D_address=0x40;
//-------------------
//for colormeter
#include "Adafruit_TCS34725.h"
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);
//-----------
ADC_MODE(ADC_VCC);
int vcc;
double dvcc;
//-------------------
#define HOSTNAME "Coldframe"
const char *hostname = HOSTNAME;   //for OTA
// Set to true to define Relay as Normally Open (NO)

String SinkString="OFF";

#define RELAY_NO    false
// Set number of relays
#define NUM_RELAYS  2
// Assign each GPIO to a relay
int relayGPIOs[NUM_RELAYS] = {FAN_PIN, MOTOR_PIN};
String devices[NUM_RELAYS] = {"Fan", "Lid"};
IPAddress ip(192, 168, 1, 23);//pick proper ID in your network
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 235, 255, 0);
IPAddress dns(192, 168, 1, 1);

// Replace with your network credentials
const char* ssid = "TP-Link_25D8";
const char* password ="610264317";

//--------Voor AP---------
// Set AP credentials
#define AP_SSID "ColdframeAP"
#define AP_PASS "secret"
//---------------------------------

//analog chip
#include <PCF8591.h>
PCF8591 pcf;



OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);// Pass address of our oneWire instance to Dallas Temperature.
DeviceAddress Probe01 = {0x28, 0xFF, 0x11, 0x50, 0x04, 0x17, 0x03, 0xDF};// NOT USED
DeviceAddress ProbeSoil = {0x28, 0xFF, 0xD4, 0x36, 0x04, 0x17, 0x04, 0x35}; //
DeviceAddress ProbeCompost = {0x28, 0xFF, 0x4C, 0x67, 0x03, 0x17, 0x05, 0xCF}; //not used 
DeviceAddress ProbeBinnen =  {0x28, 0xFF, 0xD7, 0x06, 0xC2, 0x16, 0x03, 0xF6}; // 
DeviceAddress ProbeBuiten =  {0x28, 0xFF, 0xB3, 0x89, 0xC2, 0x16, 0x03, 0xBB}; // temp lokatie ROM = 28 FF B3 89 C2 16 3 BB  BAK buiten
DeviceAddress ProbeDevice =  {0x28, 0xFF, 0x12, 0x18, 0xC1, 0x16, 0x05, 0x12}; // s  INTERN in SCHAKELING
DeviceAddress ProbeReservoir =  {0x28, 0x0F, 0x2A, 0x28, 0x00, 0x00, 0x80, 0x9F}; //reservoir temperatuur 
float temperature_vijver;
float temperature_grond;//waterproof
float temperature_compost;//waterproof
float temperature_koudebak; //not used
float temperature_lokatie; // 
float temperature_internal; // bij de electronica
float temperature_reservoir; //heat storage
bool freezeFlag=1;

bool klepstate = 0;
String klepstring;
uint16_t licht;//lux;
float turbidity;
byte leaf;
float solar;
int rain;



//float HTUhumid;
float HTUhumid;
unsigned int htuhumid; 
String formattemp;
float HTUtemp;
int soilMoisture;
byte maxno=0;
String nu;

String GAS_ID = "AKfycbz6N9EpbTRkpc2F-cHaV7eB_prBCF3kYQCOsWACV57f_cC_kAP"; //googlesheet
const char* fingerprint = "46 B2 C3 44 9C 59 09 8B 01 B6 F8 BD 4C FB 00 74 91 2F EF F6";
const char* host = "script.google.com";
const int httpsPort = 443;
byte gscounter = 0;
//for secure Googleclient
//#include <WiFiClientSecure.h>
String readString;
WiFiClientSecure gsclient;

#define BH1750_address 0x23 //default or when pulled low. 0x5C When pulled HIGH

//-------for sntp
// https://github.com/SensorsIot/SNTPtime
#include <SNTPtime.h>
SNTPtime NTPch("nl.pool.ntp.org");
strDateTime dateTime;
//for timeserver
byte actualHour;
byte actualMinute;
byte actualsecond;
int actualyear;
byte actualMonth;
byte actualday;
byte actualdayofWeek;
//int dayofyear;
int d;
uint16_t actualdayofYear;//actualdayofYear
const uint8_t daysInMonth [] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

String maandNaam [] ={"Jan", "Feb", "Mrt", "Apr", "Mei", "Juni", "Juli","Aug","Sep","Okt","Nov","Dec"};

//----------------
//for color sensor
uint16_t r, g, b, c, colorTemp, lux2;


// REPLACE with your Domain name and URL path or IP address with path
const char* serverName = "http://192.168.2.8/post-esp-data.php";//address of your Mysql server

// Keep this API Key value to be compatible with the PHP code provided in the project page. 
// If you change the apiKeyValue value, the PHP file /post-esp-data.php also needs to have the same key 
String apiKeyValue = "tPmAT5Ab3j7F9";

String sensorName = "HTU21D";
String sensorLocation = "Coldframe";

const char*apiKey="LKGGMGJYYTD32M"; Thingspeak WriteApi
const char* resource = "/update?api_key=";

// Thing Speak API server 
const char* tserver = "api.thingspeak.com";

//-----------------
//for smoothing
const int numReadings = 10;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average
boolean flag=0;
