void sendData(int x, int y, byte z)
{

  // //Serial.print("connecting to ");
  // //Serial.println(host);

  gsclient.setInsecure();
  if (!gsclient.connect("script.google.com", httpsPort)) {

    // //Serial.println("connection failed");

    return;
  }

  if (gsclient.verify(fingerprint, host)) {

    // //Serial.println("certificate matches");

  } else {

    // //Serial.println("certificate doesn't match");

  }
  String string_x     =  String(x, DEC);
  String string_y     =  String(y, DEC);
  String string_z     =  String(z, DEC);
  String url = "/macros/s/" + GAS_ID + "/exec?temperature=" + string_x + "&humidity=" + string_y + "&moisture=" + string_z;
#ifndef ESP01
  // //Serial.print("requesting URL: ");
  // //Serial.println(url);
#endif

  gsclient.print(String("GET ") + url + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "User-Agent: BuildFailureDetectorESP8266\r\n" +
                 "Connection: close\r\n\r\n");
#ifndef ESP01
  // //Serial.println("request sent");
#endif
  while (gsclient.connected()) {
    String line = gsclient.readStringUntil('\n');
    if (line == "\r") {
#ifndef ESP01
      // //Serial.println("headers received");
#endif
      break;
    }
  }
  String line = gsclient.readStringUntil('\n');
#ifndef ESP01
  // //Serial.println(line);
#endif
  if (line.startsWith("{\"state\":\"success\"")) {
#ifndef ESP01
    // //Serial.println("esp8266/Arduino CI successfull!");
#endif
  } else {
#ifndef ESP01
    // //Serial.println("esp8266/Arduino CI has failed");
#endif
  }
#ifndef ESP01
  // //Serial.println("reply was:");
  // //Serial.println("==========");
  // //Serial.println(line);
  // //Serial.println("==========");
  // //Serial.println("closing connection");
#endif
}
