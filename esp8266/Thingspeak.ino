void thingspeak()
{

 
  
  // //Serial.print("Connecting to "); 
  //Serial.print(server);
  
  WiFiClient client;
  int retries = 5;
  while(!!!client.connect(tserver, 80) && (retries-- > 0)) {
    // //Serial.print(".");
  }
  // //Serial.println();
  if(!!!client.connected()) {
     // //Serial.println("Failed to connect");
  }
  
  // //Serial.print("Request resource: "); 
  // //Serial.println(resource);
  client.print(String("GET ") + resource + apiKey + "&field1=" + String(HTUtemp,1) + "&field2=" + String(HTUhumid,0) + "&field3="+String(soilMoisture)+"&field4="+String(temperature_grond,1)+"&field5="+String(temperature_lokatie,1)+"&field6="+String(temperature_internal,1)+"&field7="+String(temperature_reservoir)+"&field8="+String(licht) +
                  " HTTP/1.1\r\n" +
                  "Host: " + tserver + "\r\n" + 
                  "Connection: close\r\n\r\n");
                  
  int timeout = 5 * 10; // 5 seconds             
  while(!!!client.available() && (timeout-- > 0)){
    delay(100);
  }
  if(!!!client.available()) {
     // //Serial.println("No response");
  }
  while(client.available()){
    Serial.write(client.read());
  }
  
  // //Serial.println("\nclosing connection");
  client.stop();
}
