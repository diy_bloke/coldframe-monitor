/*By diybloke https://arduinodiy.wordpress.com/2021/04/19/cold-frame-monitoring-with-an-esp8266-webserver/
  The relay and mysql code based on project of Rui Santos
  Those project details at https://RandomNerdTutorials.com/esp8266-relay-module-ac-web-server/

*********/

// Import required libraries
#include "ESP8266WiFi.h"
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include "ESPAsyncWebServer.h"
//#include <ArduinoOTA.h>
#include <ESPAsyncTCP.h>
#include <AsyncElegantOTA.h>;
#include <WiFiClientSecure.h>
#include "config.h"


const char* PARAM_INPUT_1 = "relay";
const char* PARAM_INPUT_2 = "state";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
AsyncEventSource events("/events");// voor SSE

signed long lastTime = -10000;
unsigned long timerDelay = 10000;  // send readings timer: 10sec
unsigned long msqlDelay = 15 * 60 * 1000; //15 min  ook voor Thingspeak
unsigned long lastmsql = 0;
//-----for reconnect-----
unsigned long previousMillis = 0;
unsigned long interval = 5 * 60 * 1000; // check every 5min
//-----------

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href='https://use.fontawesome.com/releases/v5.7.2/css/all.css' integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="icon" href="data:,">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    h6 {font-size: 1.0rem; font-style: italic; vertical-align:middle;}
    p {font-size: 1.0rem;}
    p.solid {border-style: solid;}
    sensor-head {font-size: 12px; font-style: italic;font-weight: bold; vertical-align:middle;}
    sensor-labels {font-size: 6px;vertical-align:middle;padding-bottom: 10px;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 89px; height: 34px} 
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 34px}
    .slider:before {position: absolute; content: ""; height: 26px; width: 26px; left: 4px; bottom: 4px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 68px}
    input:checked+.slider {background-color: #2196F3}
    input:checked+.slider:before {-webkit-transform: translateX(26px); -ms-transform: translateX(26px); transform: translateX(52px)}
    .slider2{width:300 px}
  </style>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
  <link rel='icon' href='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAS0lEQVR42s2SMQ4AIAjE+P+ncSYdasgNXMJgcyIIlVKPIKdvioAXyWBeJmVpqRZKWtj9QWAKZyWll50b8IcL9JUeQF50n28ckyb0ADG8RLwp05YBAAAAAElFTkSuQmCC' type='image/x-png' />

</head>
<body>
  <h2>Coldframe</h2>
  <!--
  <p>Position  <span id="servoPos"></span></p>
  <input type="range" min="0" max="180" class="slider2" id="servoSlider" onchange="servo(this.value)"/>
  <script>
    var slider = document.getElementById("servoSlider");
    var servoP = document.getElementById("servoPos");
    servoP.innerHTML = slider.value;
    slider.oninput = function() {
      slider.value = this.value;
      servoP.innerHTML = this.value;
    }
    $.ajaxSetup({timeout:1000});
    function servo(pos) {
      $.get("/?value=" + pos + "&");
      {Connection: close};
    }
  </script>
  -->
  %BUTTONPLACEHOLDER%
<script>function toggleCheckbox(element) {
  var xhr = new XMLHttpRequest();
  if(element.checked){ xhr.open("GET", "/updater?relay="+element.id+"&state=1", true); }
  else { xhr.open("GET", "/updater?relay="+element.id+"&state=0", true); }
  xhr.send();
}</script>
<p><b><i>HTU21</i></b><br/>
    
<i class="fas fa-thermometer-three-quarters" style="color:#ff9e8a;"></i> <span class="sensor-labels">Bak temperatuur</span>
    <span id="temperature">%TEMPERATURE%</span>
    <sup class="units">&deg;C</sup>
    <br/>
    <i class="fas fa-thermometer-full" style="color:#f00;"></i> <span class="sensor-labels">Max</span>
    <span id="max">%MAX%</span>
    <sup class="units">&deg;C</sup>
    <br/>
    <i class="fas fa-thermometer-empty" style="color:#00f;"></i> <span class="sensor-labels">Min</span>
    <span id="min">%MIN%</span>
    <sup class="units">&deg;C</sup>
    <br/>
   <i class="fas fa-tint" style="color:aqua"></i> <span class="sensor-labels">Vochtigheid</span>
    <span id="humidity">%HUMIDITY%</span>
    <sup class="units">&#37;RH</sup>
  </p>
  
   <i class="fas fa-x-ray" style="color:blue"></i> <span class="sensor-labels">Heatsink fan:</span>
  <span id="sink">%SINK%</span></p>

    <i class="fas fa-battery-full" style="color:#204"></i> <span class="sensor-labels">Vcc</span>
    <span id="vcc">%VCC%</span>
    <sup class="units">V</span>
  </p>
  <p><i><b>DS18B20</b></i><br/>
  
    <i class="fas fa-mountain" style="color:brown"></i>
    <span class="sensor-labels">Grond</span>
    <span id="temperature2">%TEMPERATUREGROND%</span>
    <sup class="units">&deg;C</sup>
    <br/>
    
    <i class="fab fa-skyatlas" style="color:skyblue"></i> 
    <span class="sensor-labels">Omgeving</span>
    <span id="temperature5">%TEMPERATUREOUTSIDE%</span>
    <sup class="units">&deg;C</sup>
    <br/>
     <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAASc3pUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZppkuy6boT/axVegjiTy+EY4R14+f6SVFVXD/c9R/h1xylVa+AAJBIJ6Fzzf/57Xf/FTzB3vnxIOZYYb3588cVWvuT7/NT9aW6/P8/Peo7m+/nrfcFyynF0588cn/tf5817gHOofAsfA+X+XGjfLxT/jJ9/DPRM5LQiy5fxDFSegZw9F8wzQD3bumPJ6XMLbZ7j8/wxQ9bW+HBpj/0e5OffPmG9ETjprJ3OuJtP6+xZgNM/c7nKF8On4SbWzW913kU+nTPPSjDIX3a6P1Z1/fLK65v5h/M/nOLiOX9x4rsx4/v453kT/jb+tU38MbPr75m/nW/G2J/bef1ba+RrrXl2V33EpPHZ1Gsr+xs3Nkzu9mOR38S/wPe0fwu/+QK9He+Mu9+N326KsbhlGW+GqWaZuY/ddJbo7bSJo7UdR+lcdskW29194S2vX7NscsUNl3Fmx72Os/a9FrPnLXu6bjITD8Od1jCYERQuffwnfv9xoLUEeUNcv23FuqwMzjLkOX1yFw4x64WjsA38+v35I786PBi2mTMbrHc7Q7RgHmxdG8xytOPGwPGEhUnjGQATMXdgMcbhgTsaF0w0d7I2GYMdM/6pDJSt87bhAhOCHazSeucizslWc/NMMvteG+w5DWfhiEAYJVxTXMVXHmIDP8lnMFSDCz6EEEMKOZRQo4s+hhhjiiK/mlzyKaSYUsqppJpd9jnkmFPOVy65Flsc5BhKLKnkUkqtTFoZufJ05YZam22u+RZabKnlVlrtwKf7Hnrsqeerl16HHW7AEyOONPIoo04zgdL0M8w408yzzLqA2nLLr7DiSiuvsurba+Y6bv31+3/3mnl5zW5P6cb09hqPpvQawohOgnyGx6w3eDzJAyIw+ezOxnt7yXXy2V0sUREsqwxyzjDyGB7009iwzNt3X5775rfL+/+X3+zLc5dc95/w3CXX/YPnfvvtD68NZZt+u2t7SGEoo96O8FvWV5tZj791yDXebqQ5mcraNQZrfK4YH/Sslo2NlAnzGLY0z9ykHaaoo+a0emKtiy/OhJxCK79PetJbYDzjr/5Me//zsa5ZIAwBL8LM06wZbc0hrGHdYv+DHV7drd5ZYRxAhQU1n1MNHtj5FBlpDt+5uGfNX3slRWTTAx88c88MjmJhK66eBUo84D8MysahecZqtrOe4poFCH3OPqHj5Wsfq/m+tFI3pr/0pc0+WGdknSGLGtuqYGlAVOHcjQt091q6lR2WonMzNc+XRHbIV5UZODFsqyXEEgar7SGXGsP2yl7pvz/ifj99MWQNGSWQRmxNBt+hZ/Z6RsdVCxivuZojfHpMpcxl3TNtXzfzXrNHVhLKcHuxblWsn1aRbhj9F6BWlH3c/gzVYt+2L9lLSaJU30u9l64QmeBlDM3Tbe2RRRnfJ+S9kgvB8Xecx0IAezpNbGu4RmiCAisf3YAEZBAzezbRe0umpU7IjJYqtEJAb9ea2PzK0ZWJMYoR47SrzJbntMQiFpgDS2xzOFkmVkIsHCj68YJiC7PANT/uv/YDec2+2n6kTA55HSTX5uSYEE1NRDGrDu9JyliEdsT9rsOXUC0e42JwebJgBVp73SGRwz3nBhAzyortefD9HPHZxiy4n0l+b6p0qBEsplXF8Cnr0TxCxlYKirO1iK+/ba1BsoZMme0U0YZRIQaXO9AtFeLoAeTv8HQvnzglg+34kkUA1w6wgBPFx8yGGO6umqo4xT2sFlGEY+Vr4ex422e7lLPmhiUY7tdEcTSfKsFfSxwbC3WbOrHbAt8kH6AwJBoJIZBVGn9sgN4GDRUZp43NR+ClDU6wr4Lxgrc34jiDLRAKg7VQMcL0gvvxZSadbZuthX3uNb2tV1WMR6g8Y4/CfFnJDSZv4R47KkNvpgXfynIulQhGTR41zRaguUYRRBD2cq0ihzCJCIG9aSbwN7UOmKzPIuMT0bAqYrH3FMFlMYBlNOCKUo21bxyNzlD2YCKFDUd9JvyJKQuBdjfABzi35bizP2AwH2evr9PByICYzwzYMWEv6aZpcK/LMr1WOQoA//CJpnN1kiALEiI5DxmArp68OEOub+N45n5AJNd/wAiKj3ZuSCwx5rrmxFj4sMI5h7f/yCuV5Koty4mDB/usH2CxGu46a7uFxGSqS0p3orJyMLIHwuIynp0hecnxFjKo36thuw6Kiu4iZ2XFyj49Yq1s9BBHcGCInLWnZ8FkyMLfRum/4bvZkykdn+V6GJKJ6sg4IJKR2eXoyBXQ4jRSJHMMjgzdNUNdwb44alY4CgUgPK4L0ENV8G8aJYnmNZtjrURv3OYmSdZNJnxODDV9bJZn1jeoXG+svCz7GXAn3Ai2lsZsZRyTt23yQCCHgWamrubyNeEi1Ezq5BIsdUwJVtIKPUEqNYSuVAAnja6ttOOcpnpnW9OT9Ze5XrY35/w+K1Nv5DTlWxIf2Z/AjV32uo+99nNwhp/8pDmwUcY6LZGfyHwE/zzI6WPbrbZtN5yE3XS54jO/afgEpsISRsNGv/1CAvRZm2nfvIVGqQ0uv02B1fNesx8H232BI8goJFcj3D5vFCQmIY+ZDg6wVB2RXBeG33ROEmfvSE7LrfeAL5Nw6lO/UC6NoNzU2u8BkkbULDUaeUjz8Q+GOxf26U1pusJGnTDS2ZrHm8HwT2cgtrJaObdW7QCNhpcNvKYNAncraSNQIKP9W5LEdm2YfgdpCAaRw06UzscS0eCDLSVMQUAUlDwDYStyVe5BCYuBnJRHh/3LkQFKpjvCM8oruK9Z3VTd0JdppQOwbBBuWHAnS1Y02mE4rAhgedjuPEQEjLq2tUaH4a0RgxbyRi42FVxB2upyLn4sj9B6lBexNWavsHzure5ezHTFoiJDbH8EKlEIPmIQfFbeyIZRWGuyHUnuiSdli+BaMxk6BL2NOKlaph9YD/NIl2etFX6BZBJ0cVkCSgKZAPsl03kWTa4FI7PqoV70b4cAb5lb6ZJI3l6+Xm52L/6cacd+VOy37WbwRSZo6CocjnbnsDwp3s+thvAZV66DGVBnSJUSkuA3HhlZJI+kVpBn1mdkt+QZai4iBBryJ2WnqLHS6pTrWxhF7C2pju2xLTbLHzSG4AIJPzMeevRdifT7+soUmDoP/JUj99g7k+8KSExkLbu5DmhsqNyMo5SQXzuTzrv2PPcRB06Ka2uYo7icpBOFJx5TiEVUqUjFHk44sGNb6/ZsDTUCA0/YpauiA021dGVHRMsdkeMJWqJCrZLUGJjC09rybOJuLY8ZW0iZEDElIs5uVjnwBqZJRPB9mJDgfjT11xoLeGJH3qvQxeZgF5q8NvTHLU56sFvgmAHGqHB86PdU4tqyH0iRnLNkYxPXUrJWpfFb+72OzITTDTXUDjML4m5GwoXeoI44JDKnO04ZuUEBBIBDdkCagSq/m3hfDrnZzHsmv2e6q+rhncgzFYDZkwWJbwjKlvRahlbxrEFitEJw7HjGWxh93/5eddy3V25fQk2+qcWpf/MEmgH1ZZ0Z87qnWlEgKIbvQcUugbP60LgAD2Fg508J2OMuAQnho/AgTnNJKLidTmUfZa5CCmNlzorgrIBEtgit+zsDU2/KnSAV+9S41LV+wgkqs5oULH/1W22HlkQZlLAgGbXuy7tM3MmIqvncHm6qoilSUfE/rj/djqT+Gw6seXzIscmYfRz3m8f9gJaMW+8TOW4nHXZmi6AkF8Kwksr+NMwTstIp049t1Rav2OOdC0W4Q8WgepFONniKA0TvM/IZ92vUqqX0QGV5YGF09Vq3cNGV/HZZZe0p4Lme4L/GbNthYScJCtS86eWsf5zwyUD+gl1uJWB1hiB5QvbGEaPZAjOLBG8n4Yf4CsX9JaMOtfTryGTbSru93Gu9MePGJMKjO6ugbhEiRZeDlQaBBCWuTY8NEsjvSsovrCRjKHVuPA9JbDvYBKLXWiuSeoiHaphdVm9KsJG0aO8DP0qVa0RsT9ScKeSxtSfB9B+9j+pfrQ/iE8GG5Xbu+Lrjet1yyE69bBXhK8UoXYO+KqI29AbZHwXQJN/UPLjvg1NS4W7YXGolBjFoqlJNCEZyENnRijqYV3mgSXFndlezEtlTgBIXRxaS5ajXyFTplct2hqCKBigUDWUXDZ/aJmCNHYlKoumoJ6phkvpNddR6yzAdqLQRdICynfnG/coPACjP/RCxiIXQzDhhqB+jGiMhMKHfC5F3k7uoCieZfJKFN48oEiBFdVITyf1QBPInfMkf8yl/wmXfBPR1nHk+iUyinQcKIrMgldniDEY5EIlHgX9L5ikmqbLNkZmE9paZd321Y34U6K0XPNqsSYwZHRSpns94V/5XrBF/LBC34EpkU6PMJ91HVbjrXeGCuoqT1YN0+AnKPG4vRdIclUQWqYieYdrdnUEHrKRAmgdx5Hp5rW+OygoMd1jKAZhlKGonPMl3atqZJaJ6tu72u0uYb2jx0LdwYE8FWD9h8iF5cN+pUy7/LlQAT34H96inWTk2Ex1hi0nX6+yX3F194DAH1VrkKajukWFKPBzzrTh7VA3k9F3UfCt+qWlhD2wGmqRTIeWwKyFYkkmJELcNgag24qv180ooCTGRJyUECaZT4AN2qpz+C1RkQzYyH4a8d3USgpw9k1Fdd85dEtvkbxX87K7gMOEjvMsYcuEpY9CQtSLDdlpgcqfuSpQ8JbGrOEZmvKR28lKqFNaJQkEqB3k0H6/4R18reCPohH4KFIfvkFtId/TRIbQwS6EKG9+rMBVhrlGCQOiqa27VYtNUSyXWNllRSKur2K+bILTpRwWDo1Xdb8qzHUj1OgaaDpMgVKvAOVX6fRRd1666ELOYyBewSV5S5+5UKEmKzVFWuUcq+93O0aPn+G7phKvutp7E5Tm++CJX86V9P3PPqz+jGHp1TJoSpFomDToXvtpn2ZeeekAXSdo/WdTcxnzIgOsgE3kprfato1VgoNonO+1E7mKfSz0K8veOITKLoz48LZY1r+8tlnvWtVtk3ytX2RBoNTXC8npj6BgDIUSZft1CyX4ADBswiVAlixOsJTQ5qeCvLZH9Tkpqe1Xoqszoay74H8lIUFz7FQPyFIC43xgy57ROKpT76U8MLWO/mFBnRGaK8RLE9RLrJJd3WxEYAJ2RkcFjOHzYkXakStKdU6/vKR3F1zyIoa4pq6MPBFwDV0fYxRPQvhjCjgqO6kopWA/t2ubwsZ+wC9sqnOt9THPlOYF5zLvkBK8Bvd3IVtPvcoFN1PaLodQ+soHcEvTarltqt8vKmGmqq0rKh2WZkJSNu6fe4bBlONcilQLub1bd1lMVqg3qvs5czyk5TSCSZP5QzNAWxWejbIGUCuu+UfelG+QjSgKqqlo0inBdo58AGb69eGLcaCzwqGYa4sQR/tQKp8yXqNh8iWqOWT24wPrStNezfKtzKFLS/NkRcNx7CurAbxaIn7vS/wf55p3rcQ/Eb3+x7Ov4Uy+ml2CJT1yytOs0OvORYSXF00VB+sGfpDuquk29RvlBJbRe6u53XUGVgappuKJuPsI+RxLjeL3DVNmZJEA7E6MDSite/yNBzW7Rtt5RpPTxjmLXjggt2AyZiWiu6hI2lHoE04MHkWzq73FBim5s3ZaUCFBI3QNxMsld0Nvk6Etk4xCijct266sg2btBmOeGBnNRM6ptF3PQy40Vjqe93KSmtkEef0iCx7PRC7CWtG4qD+r9THwEDguz22cscr+jGXuLevQ6z+KYqvxa9KLg94zheW/zfcKJngSUSBUI7vK/NoJrt+j+tg7gQb3i1D7CzQLv6YPXpQ2PSi3yff4YZzXOJzdI49iwIcb6VpiToo8d//EOtgzT47Vsz8bc6oeQh6ves1Lznqi7Vbk29YPCohAjra79suvd3ylW//2leHTxhfOcJOKtdoLZGq/t13HnXe1RXKaNuZdwWndS04AP6TjDebXr8mV8K3o+3739fsGquqsZUv1E4nuVFYkUzFaVP1SaPPWTKZcKKAQlA6njvmvA90Vdy26XeNK9FHhKS13vOffNfFHgkIP9vAgJtsqaK/L+j4D9F8dkEcw53Tl3iK1O5QAV7UFM/CQBkrARqRLvX20Du9sG90DpUiBG3/T/AigLkO6lX3s1M0ep+NOWIaDtY061D/325POm+5Wj92s/GVlVCSkJxba5IDQC7f062alBV5V2sloGazfsgn3cfjJWtG33M08i4+T1PnveAH7M+y+OetO/NcWW8/tF/xX0FlRl5DeeICzL6/Uze1MNSaEnU66XTFxqo9z39b9/BNsWro93DgAAAYVpQ0NQSUNDIHByb2ZpbGUAAHicfZE9SMNQFIVPU0UpVYdWEHHIUJ0siIo4ShWLYKG0FVp1MHnpHzRpSFJcHAXXgoM/i1UHF2ddHVwFQfAHxM3NSdFFSrwvKbSI8cLjfZx3z+G9+wChUWGq2TUBqJplpOIxMZtbFXte4UMAgwihX2KmnkgvZuBZX/fUTXUX5VnefX9Wn5I3GeATieeYbljEG8Qzm5bOeZ84zEqSQnxOPG7QBYkfuS67/Ma56LDAM8NGJjVPHCYWix0sdzArGSrxNHFEUTXKF7IuK5y3OKuVGmvdk78wmNdW0lynNYI4lpBAEiJk1FBGBRaitGukmEjReczDP+z4k+SSyVUGI8cCqlAhOX7wP/g9W7MwNekmBWNA94ttf4wCPbtAs27b38e23TwB/M/Aldb2VxvA7Cfp9bYWOQIGtoGL67Ym7wGXO8DQky4ZkiP5aQmFAvB+Rt+UA0K3QGDNnVvrHKcPQIZmtXwDHBwCY0XKXvd4d2/n3P7tac3vBy8ycozXcU5bAAAPnGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6aXB0Y0V4dD0iaHR0cDovL2lwdGMub3JnL3N0ZC9JcHRjNHhtcEV4dC8yMDA4LTAyLTI5LyIKICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIgogICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgIHhtbG5zOnBsdXM9Imh0dHA6Ly9ucy51c2VwbHVzLm9yZy9sZGYveG1wLzEuMC8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgeG1wTU06RG9jdW1lbnRJRD0iZ2ltcDpkb2NpZDpnaW1wOjc5MmNlYjVjLTNiNzQtNDE1OC05NDczLTE1YmUwZWY5MjczZSIKICAgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpkNjliODI2My0xNjRmLTQzY2UtOTE2OC0wODUxZDE3ZmRlMjciCiAgIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDozMGEwNTkyNi1jN2Q2LTRiMjItODE2Mi1jMjNlYmUzMmJmNGMiCiAgIEdJTVA6QVBJPSIyLjAiCiAgIEdJTVA6UGxhdGZvcm09IldpbmRvd3MiCiAgIEdJTVA6VGltZVN0YW1wPSIxNjEwNDA4NzkxODgzNzQ2IgogICBHSU1QOlZlcnNpb249IjIuMTAuMjIiCiAgIGRjOkZvcm1hdD0iaW1hZ2UvcG5nIgogICB0aWZmOk9yaWVudGF0aW9uPSIxIgogICB4bXA6Q3JlYXRvclRvb2w9IkdJTVAgMi4xMCI+CiAgIDxpcHRjRXh0OkxvY2F0aW9uQ3JlYXRlZD4KICAgIDxyZGY6QmFnLz4KICAgPC9pcHRjRXh0OkxvY2F0aW9uQ3JlYXRlZD4KICAgPGlwdGNFeHQ6TG9jYXRpb25TaG93bj4KICAgIDxyZGY6QmFnLz4KICAgPC9pcHRjRXh0OkxvY2F0aW9uU2hvd24+CiAgIDxpcHRjRXh0OkFydHdvcmtPck9iamVjdD4KICAgIDxyZGY6QmFnLz4KICAgPC9pcHRjRXh0OkFydHdvcmtPck9iamVjdD4KICAgPGlwdGNFeHQ6UmVnaXN0cnlJZD4KICAgIDxyZGY6QmFnLz4KICAgPC9pcHRjRXh0OlJlZ2lzdHJ5SWQ+CiAgIDx4bXBNTTpIaXN0b3J5PgogICAgPHJkZjpTZXE+CiAgICAgPHJkZjpsaQogICAgICBzdEV2dDphY3Rpb249InNhdmVkIgogICAgICBzdEV2dDpjaGFuZ2VkPSIvIgogICAgICBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjFkYmIyZDk1LTgxZjItNGFjOS1hYzg0LTM1NjQyZWYyYTFkNiIKICAgICAgc3RFdnQ6c29mdHdhcmVBZ2VudD0iR2ltcCAyLjEwIChXaW5kb3dzKSIKICAgICAgc3RFdnQ6d2hlbj0iMjAyMS0wMS0xMlQwMDo0NjozMSIvPgogICAgPC9yZGY6U2VxPgogICA8L3htcE1NOkhpc3Rvcnk+CiAgIDxwbHVzOkltYWdlU3VwcGxpZXI+CiAgICA8cmRmOlNlcS8+CiAgIDwvcGx1czpJbWFnZVN1cHBsaWVyPgogICA8cGx1czpJbWFnZUNyZWF0b3I+CiAgICA8cmRmOlNlcS8+CiAgIDwvcGx1czpJbWFnZUNyZWF0b3I+CiAgIDxwbHVzOkNvcHlyaWdodE93bmVyPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6Q29weXJpZ2h0T3duZXI+CiAgIDxwbHVzOkxpY2Vuc29yPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6TGljZW5zb3I+CiAgPC9yZGY6RGVzY3JpcHRpb24+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgCjw/eHBhY2tldCBlbmQ9InciPz7bRMiIAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5QELFy4f/1o/DAAAATxJREFUKM+N0ztInEEUBeBvdRFcIrEQRBBCQDTBQjTYqIWtQoSslYWFSLYI6WwESaddisROEMVOEcUyVgYrGy1sguYBIUVW+yyEELC5f/gddpdcGObO48x9nDMFje0pJlDAFQZwHgO01gHNYg0P8RlVDGMBD3CKv1BMgO/xFeVk/yNq6MQWXsb6ny3nQF0oJQ+8xTM8wi605C4/xmGsy3gSfj/28SXq/Y4TTGWvrmA0F6WCcbzBNnqSLNqz6LCXHFZwhukmbBwVsYGxmPM0vcJFE/DN/0Yu1WngQdawy6RmWAp+dzCD+aTmX3Ld3kwij4Q/iR9JdhW8yBRWizq78Sno+Ik/mMNxKK6KtgCvFpJU3+Eb1us0qBIKG8oUlmr7A3qxij50BMfPsYhbvMZv8WMa2WBQWMB1KO3er7oD3289l2XeOWoAAAAASUVORK5CYII='>
  
    <span class="sensor-labels">Apperatuur</span>
    <span id="temperature6">%TEMPERATUREINTERN%</span>
    <sup class="units">&deg;C</sup>
    <br/>
    <i class="fas fa-prescription-bottle" style="color:red"></i> <span class="sensor-labels">Reservoir</span>
    <span id="temperature7">%TEMPERATURERESERVOIR%</span>
    <sup class="units">&deg;C</sup>
   
  </p>
  <p><b><i>BH1750</i></b><br/>
    <i class="far fa-sun" style="color:gold"></i> <span class="sensor-labels">Licht</span>
    <span id="lightnew">%LIGHT%</span>
    <sup class="units">Lx</sup>
  </p>
  <p><b><i>PCF8591</i></b><br/>
  <!--
    <i class="fas fa-water" style="color:slategrey"></i> <span class="sensor-labels">Turbidity</span>
    <span id="turbidity">%TURBIDITY%</span>
    <sup class="units">&#37;</sup>
    <br/> 
    -->
    <i class="fas fa-water" style="color:sandybrown"></i> <span class="sensor-labels">Soil Moisture</span>
    <span id="moisture">%MOISTURE%</span>
    <sup class="units">&#37;</sup>
    <br>
    <i class="fas fa-leaf" style="color:forestgreen"></i> <span class="sensor-labels">Leaf wetness</span>
    <span id="leaf">%LEAF%</span>
    <sup class="units">&#37;</sup>
    <br/>
    <i class="fas fa-solar-panel" style="color:#c71585;"></i> <span class="sensor-labels">Solar PV</span>
    <span id="solar">%SOLAR%</span>
    <sup class="units">V</sup>
    <br />
    <i class="fas fa-cloud-showers-heavy" style="color:mediumblue"> </i> <span class="sensor-labels">Regen</span>
    <span id="rain">%RAIN%</span>
    <sup class="units">mm</sup>
    <br/>
  </p>
  <p><b><i>Klep</i></b><br/>
    <i class="fas fa-box-open" style="color:green"></i> <span class="sensor-labels">Klep</span>
    <span id="deksel">%KLEP%</span>
  </p>
  <p><span id="tijdnu">%TIJD%</span></p>
  <!-- code voor event -->
  <script>
if (!!window.EventSource) {
 var source = new EventSource('/events');

 source.addEventListener('open', function(e) {
  console.log("Events Connected");
 }, false);
 source.addEventListener('error', function(e) {
  if (e.target.readyState != EventSource.OPEN) {
    console.log("Events Disconnected");
  }
 }, false);

 source.addEventListener('message', function(e) {
  console.log("message", e.data);
 }, false);

 source.addEventListener('temperature', function(e) {
  console.log("temperature", e.data);
  document.getElementById("temperature").innerHTML = e.data;
 }, false);

 source.addEventListener('max', function(e) {
  console.log("max", e.data);
  document.getElementById("max").innerHTML = e.data;
 }, false);

 source.addEventListener('min', function(e) {
  console.log("min", e.data);
  document.getElementById("min").innerHTML = e.data;
 }, false);

 source.addEventListener('humidity', function(e) {
  console.log("humidity", e.data);
  document.getElementById("humidity").innerHTML = e.data;
 }, false);

 source.addEventListener('sink', function(e) {
  console.log("sink", e.data);
  document.getElementById("sink").innerHTML = e.data;
 }, false);

 source.addEventListener('pressure', function(e) {
  console.log("pressure", e.data);
  document.getElementById("pres").innerHTML = e.data;
 }, false);

source.addEventListener('moisture', function(e) {
  console.log("moisture", e.data);
  document.getElementById("moisture").innerHTML = e.data;
 }, false);

  source.addEventListener('temperature1', function(e) {
  console.log("temperature1", e.data);
  document.getElementById("temperature1").innerHTML = e.data;
 }, false);

  source.addEventListener('temperature2', function(e) {
  console.log("temperature2", e.data);
  document.getElementById("temperature2").innerHTML = e.data;
 }, false);

  source.addEventListener('temperature3', function(e) {
  console.log("temperature3", e.data);
  document.getElementById("temperature3").innerHTML = e.data;
 }, false);

 source.addEventListener('temperature4', function(e) {
  console.log("temperature4", e.data);
  document.getElementById("temperature4").innerHTML = e.data;
 }, false);

  source.addEventListener('temperature5', function(e) {
  console.log("temperature5", e.data);
  document.getElementById("temperature5").innerHTML = e.data;
 }, false);

 source.addEventListener('temperature6', function(e) {
  console.log("temperature6", e.data);
  document.getElementById("temperature6").innerHTML = e.data;
 }, false);

  source.addEventListener('temperature7', function(e) {
  console.log("temperature7", e.data);
  document.getElementById("temperature7").innerHTML = e.data;
 }, false);

 source.addEventListener('lightnew', function(e) {
  console.log("lightnew", e.data);
  document.getElementById("lightnew").innerHTML = e.data;
 }, false);

 source.addEventListener('turbidity', function(e) {
  console.log("turbidity", e.data);
  document.getElementById("turbidity").innerHTML = e.data;
 }, false);

 source.addEventListener('vcc', function(e) {
  console.log("vcc", e.data);
  document.getElementById("vcc").innerHTML = e.data;
 }, false);

 source.addEventListener('leaf', function(e) {
  console.log("leaf", e.data);
  document.getElementById("leaf").innerHTML = e.data;
 }, false);

 source.addEventListener('solar', function(e) {
  console.log("solar", e.data);
  document.getElementById("solar").innerHTML = e.data;
 }, false);

source.addEventListener('rain', function(e) {
  console.log("rain", e.data);
  document.getElementById("rain").innerHTML = e.data;
 }, false);

source.addEventListener('tijdnu', function(e) {
  console.log("tijdnu", e.data);
  document.getElementById("tijdnu").innerHTML = e.data;
 }, false);

}
</script>
  <!-- einde nieuwe code -->
<!--
  <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABMCAYAAAD6BTBNAABQF3pUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarb1pkuQ41mX5n6uoJRATASwHAynSO+jl9zlQjxwis0S+7q7wcHdzM1UqCbzh3jfhev/v/+u7/tf/+l+h3fW5cqnt6c9z81/uucfBF+3+/TfOn+HO58/zX/nzI/79b9+/8vrzg8i3En+n3z/b8+f1f30//OMCv78GX5V/uVD7c6Ew//0HPf+5fvvbhf58UPKOIl/sPxfqfy6U4u8H4c8Fxu+x7qe3+q+PMN/f3/uvR2y/35d/pHqu/Y+L/P3fubJ6u/DNFOObQrr5M6b4u4Hk73ClwReBP/2x30p8nVM53+9/7oQF+W/rdP/LXV1/35V/fPW3XfnSf9+U9PxecfGNf1/M5x9//9fvh/LfF/86S/wvn5zWn6/iv3+/9Fz+/jh//f6+3a7ve39PN/LDkj5/HuqvRzxf8cLJkqfztodfld+Fr+v51fnVLqR3sTv7Xvfk1wo9RNb9CznsMMIX3vP3CotbzPGNlb9jXGyU32upxh5Xui92K/srfLGmnnZq7NxiexPfjf+4l3A+t5+PWyjXvnfglTFwsaAoXP7xf+LX//ZC36fIh3AWM5214s+oEHIb7px/8io2JHx/yVE5C/zXr7//574mdrCcZW484Ljn7xKzhD+ypRyls9GJFxb+/qlFqPvPBVgiPrtwMyGxA/cTUglPuGuMNQTWsbE/gwu1mHKcbEEoJW7uMuaUHjanRT+b99RwXhtL/H0bm3WU6EmVrelpsFcZw4b81NyQoVFSyaWUp9TSSi/jSU9+yvM89dH4jZpqrqU+tdZWex0ttdxKe1pt7Wq9jR57wjiW/vTaW+99DD50cOXBuwcvGGPGmWaeZT6zzjb7HAvxWXmV9ay62rX6GjvutLET+9l1t933eMOLKL35Le/z1re9/R0fovalL3/le776ta9/4x+7Fq7ftv7Hr//5roW/di2enfKF9R+7xltr/esSQXNS3DN2LObAjld3AIGO7tndQs7xcuvcs7tr00rkLoubs4M7xg7mN8TyhX/s3T937t/27cr5/9e+xb927nLr/k/s3OXW/W927j/37b/s2tbbrDtdZ4dUQxf1TqjfF3Of6935websPli9XWarXA178taw3rTnLNxMiG+v95jswZX3V1mNicMIb531DWW0Hsp8O2vyPrGtsZ77fdniWHtqYa9dSnv5iG+8mx+UOPZzPe/kFh72piVukadEXb862PHQnjVyS99O49t55R34Kd8KvHbVZ3015T1XG9zLFVpDrFp5WMS9Z0d875B2+0p6Z39ZtDt/uMia7pn57PfNzztSanzYmBWpeDaS0K9R33d/LPae7HvqqPzqL8s78x16fd74RR5v98xKjDUjjxYq+1I3MvSuZyI63PNVnv0iV/mbK7/vKKVj3dudEhvyBL7NlveeGyvSd2QtCy5gDuSeTUXO7rPX70DX4mj73Xu9g+efiHt/3pJbXE1rO9s7+fiJGLPbgU+bT3jWKvF9S9mrvbigcQO03po+7mCPURA3YFfPedZYyvu2xoPsOkLoL4+R5sMLWMIHy/Egd+/9NHSmPljEa863ui64ye/t5UGG15r81xGGXieKWD/W+Ekr8UEtzDx4pvud64tclycqOEPWCMhR5npGGDg8tLTnqudiJUP/uNfveb9SlXwcIdITN5BmlcDGRW4XxfrYmPcC4iFc+x480CyZW0upo3kANd44cIkPrvx+A8qavNs++PxS0ejYC5t/p7e2t1w5NZQ7KLzINZ8xdTFrxwfVYrsDjmDddfWY84vIrpU62olclDq/Z+CaR8xoPyq22EQUnXdjK7/CnmO1yox37Z2XohasIEv2oVXs3Z3KWu/EXHHTkweso+Z1oZssYxgdgS+xzY19wPLsFPN6NKs5rA8ckY9KjD36G8ZA29a+2978kOerGXu0JsqZeXg2K898NivPd775z2Zhl7AOv83qZ7Pq3KjJ03fFSyKdC62/ntBv7PPbEdwH6UdvEGU+DHu90DKUrNScHm6kzdF7wP6iR8hiu4fL+94VyWTXdlq8sxbWdvK0H8/JD8p6A3e4Un6+/X6fZjKGZw4WKcfnzYDVp3KtlYZ62a/n++6PR2fzOzDK1z5YY+4GUw+YYu0GYsRu15YjtucuGJL8rf7NGTGY65iwcc1RsNLYr9DKxNEE9qE+6Vvrm8g5FjZgKN9cwvtgWgAImE2EkLWri4WcQMfNul2I70ooI3aop/h+68ksDhcCWAA7dsJpoGZrNyQQazM/pL6W43XZsYDFQntruNhMDODLRqG07X6FxU8PkyvOG+0A2dU4Slvx06DV7xgptqZge9LNx/CI/PtCCEtDUss9t6rxpblZNnAjD7/wGXknIBGaUhabdH7GruZvVOwKxntgV76axUd7f8/CiSKGE8c02TysXx/tbUjOQm93Yx/2i7aj8bVHNhwF+7Bve8zGSpZ5FZQDe89PMoYnowO4PMxuQtDxXN7bwvDqaPCwaCFogB3QFa2Bnhb0lPWoF4uul9pPPz8DMNSAxBVUDlP9FYhAG9jsoaxudLl/YahBQNT0YnfbeyT+wmXqktFKIGHFyY8Ha4JYYT1qUs1mxhhm7vJBW/Fb6O+H5cG5A7YxZDiANAe6FvlWXaxkbqzH2p/2G53TweFOa0x4CvD/LSRCbJCdgEf+m0G5sCi4zcTqp9w2CAjHzXO9TV6aBpiH1ccTwPewqeldOYJMNB4KIGKDN1wDv7Zn2isj11jxhMygcjXgiWbC2JeOlXs+4HvRFfGT90HGP6AOkqGd58L623ZfbGAZ+UteFpzBB5Wq2grMGj+rYYM/Xmzz1jQ33St4BaEFSOMvud+o3Fx46BuXhIR9GYSCVX7jBB6BeTafHGPfd8caZLirV0V/UblS/37J65/X/NslnwLU6A13DAL4nonzgFtjlxd6hGCwo9ygeINnxmYjeW+HdHG1Fwd180mIn+ZVF4vyfWxS6fjwAZ7B9X4wPTSa1W6BD3q7MGcel826ossDuhEAdXfRZ2OQ4xd4OXuIIi5MPs4bG9dZ2zonr4jIz14Akqj3vp54I1gJY/x1bAKooU6E4UW5JfYo+gIM+sCRzd5p3rwIzLVxEzwU+xdu7Fi+2A+0lTX+EFj8rCCMt2I3Kk6JR0Pw+CieJYi30GSvg4C4ybEDTjYPCjnmxWhWgJX+0b5/KJ+IEQ3jN16GjfuwtngOnFgJ0F81iTdXoQ6e/noQv8YCPYMtwair72Gh+Efz77E1tq+ODJv9gG4rhmOgBsjcFk53LHlTabv6DqIDhXE/2OT32aGWzBocxAA04uPbMNwxJi+Mu2GZ34qlfrRqaPWe9WovJgVlu3HRPBo2QXCDlfoiPoeV1nbtZ4DmkQ+2LW2MPqK4QFIFaXp0MnlemEd2/6sagzLehtmJ+K1aR3rfF9nQdwJ/e2lfA0i/WeONI1LnJ2sPgZlh5KtNLRH7zbtQqIgk78jDsm6Y4w08mDd7hibKGwC/sJh9VFbNrh1ZT9iYeOWBOZtiiDwmahbyxim++D+Wc4OV2sT4DPQO+7M+0Fep8COuiF/wVgFCX8vjQmYmrAEJjUCJ98YzDk0N64p7Fxn3ggtEXcEKYDOINR4Z+WDva8c6tQ29gYuA5b8bWI9YlOGd+P0Hw8amJ3AQnkxk8izMCWgFJ1tZwtJkJRgntAT/uSKmtvCVCw3YgITxVO/38jHfMUUYoY1BAURsNJuV03Jit3GqbD6bOVDClmt8YUep3VwKqMYDD+6lGz4BNaCKS//zcKv4ZuiDhBCByx33hbmAp6NJYDe09r4w+SCX4rIhzsjfh494MND4gxvC8T68YWPKhog5BzQECwqJgM7iZ8B4cI27pgvDg7/A+WFTAF3pmxiewq6w6XEBUsD2DR/04LS489m4P+WXu0O1lOYkqowArVB4ECRlDpjhakDVLapu5ZVKNT0+/Btlg6nyz/C+PzIzoKWYv71BpRM54sUD82vglh1qQPD9jhesNthvkMRCvkK9UV5kCy9bX+yWmJ/7/tCNG/LCflwIQuZz3nELDbkcKw292bE11h6gCwrFhmQhF794GHA6ApoHAG5iMD7sPXbq0lj0Z+HPMh+Ec8aIopUIXWZdEq88tAgiH7Gk2PsNOkeqgTbcLeYQn4gQ12vuCi9eQwNdlTsxGE8Ni4cJNSMYNWYDlw0t5OMUeqw6OD/wuNgBvF7L+fIiIyLWyODGo4S1w3y/u2+g/I2nbUMVDtJcuFoXE/a861zc/gAPYkR5gnjhrqeiB1iIrwERpH5+ox8LVlEvnw1+1zde+GsFp45ssXIQnc6G3HwsIggYbTIOBDB9KAFkMbPC8HnkHPyAHNas3wOMsp3H/ArJOsyWZ8V4dkjjDKxR4HFL4eooGk+G+fpQ1iQW3Q/gEITW4wifxgv4AFxD1uBp0E4s7brnBKcjCtdoCeVu4AkM76ucgr9bxcHp8je7iKiK8/W7wFpISY+96gdf7iliNfjOYNeGDIb3sVD/yWCQKFHhhkfwFxJc0NWJQ0JxwSr4PPa8jxP2AbYt4FfXvkhFUJYX8gIZiC7VxE4iPjhRtLcMpPqpkpbegLd16pFBGfe+hubIqBBODxyWsDMJl4qT5DkmFqnokni+B4T8AlKTsg0cbODQeJyi8l0vYFqDHgOGZoSm419b7oZ4/wQIDKocfjxCWSDoXCAIc4kOAg6w/9DBm9G1W3iADZ0BCLNwxQAEUD3qhkmdrescYvMe2GUEGyoynoJJZkEzhglPhiO9MAKsRc6qAcAFwCPX+zl/ONWNdwY247khgLOMu6CFBXOEGcXz/xPysEZYMyMjc+LhMHKvALdnmG+BkgGODd3gTtGflUBkXGvnV2OGXYqsBx4KWwKIwN4do17x5K09QVQNgGJtXghoRCPjB33PsMOQ/1s04wQzrn9GM0a6t1wblAenRl4gELjsCuwHe/WwToA+1l32IWFwUm4MFxWhJ8/1PiFDyT+jakgrkgg/KM8Ltsa7QmUWIowDT6zvWoIgYHmYOgUWHr+H98W2Ao/1/XC1BCQsvdVvTPAs/u8XIcOzYSXwFYBLTO9T8UbAo4HYqXCYVl7HNn5X5tYg5bjIjrbgIB9hRA/w6uPzucTm/lF0YBIMB5uBIcSu46E7xgYlBwTBsqM4bNeS8EQQIO7nHflgCISd17wnAKgR+aQmuMF/AxAdABH3E7+rYwBRIsAtMsctjg8QwxrxG0eR0Dh2oRsBBnqxteAWYwpqGt4to3hxf0CZCytVckEQ/QRuPBlfeUNK3A3yhvAANrfxYYC1WcYCTp/4HWQMSwY4xXLfHd6/7ligaWxVx/nd6IEJNagg2vmF20AvcgFM5w7CB3w4FmKwsGwcnlaVq8bYKk/8gG/j0MJPjSm+4kBZPPe7g6FCcC4gApDWE48MLNo8ScWpobUdT4gc3SYfsLK8qL5ipLDNRlRRkgGue2EeoK66NNhNE/8uTKJRhsAqmsHADcVLvQZeJNwPmGBChrFDMEKQx4MpxSNhVUTk2IiAPR/D+GAxPFUxpCDmFzPzfZCapkAsFJEPxoOkzdvA4+DKqKtDr3j8AHCKolgYJJ7iYTvgGyAARJmP3wEqWpHUFY1kZORAH8CDCNk+40JhPwEcgyfG0rBkxnUwVKsBDlDH567zaMCFowZ0YqLERABw7NctxIRMVOwiMAezkw2u4HjwmEBDHBgyjqcB/RyQ1NjjC0fX+QlyxtpmxGid/AfsHPIAacrAJkxGWXjCLFWveEeDTzp5BD+yqeaGLswPOKY0M3n5Me6CAQDsqX14w/Boo58oFI6Q7Q/MWL+nIsKVfYEpJNMxY14T48werf4Ms4QTFx8Mbd2Il380bakQExjZMNi5oJz11ZJ0nAIQCSSPHbnYm1CEpwbNF5ACp/CBljsY/Q2YUqAGGxURl2cB5NoH6vqAcKDepw9oAQrDyl3zuY3cQcDeAsSG+H0gqDY+NmA934k9Y1CgJ/zMcLwp/QXnmjfCdQtffdt78QEPJPJR+dvJ9zdNs2GXe0GgsOTJO0NZ4e7gVZQGg50x8bBYYb0Zm7avZwaYeu1Zyg5Qe/UBaQGIDRYgQIUlk4eb6OBe64srA4UC5wCWeEqMlfQNM5Lc/JtdPtEShClmrBmIEsjFfSf20tCR6ZnH4GuNH0gjIpKgTLP7C8GoFygcOwTM4nfRrmaAYEc3x9YCBmXnRZyA2C/o5E14TrZZRgtwFstxMYzKxU60wMW5YAAvNUPgPVewUEQivvmVhHCf0EiH6kHGufMi6U8feozD3hXrBWL7Il4QQJ/wODe0VCsweffX8N4TbcC7mZZu7WOPIFF4y/dw3XfCYl5cC5B5YkYgzA/IEOOG03X3oXnYiefTbfJxYA62zwxJPiER4Ob9dbStstFcDUIRvnCBNUeN8rmJBzEjqKYkdgMri1JCcTCcG1HZEIinggLx8hCVmFlEFr3e3GRcgAhEWDq2BQ1tRZOHs51CC4g/yPZ7zdoN9Lxg183x6J3LcQXc5zT8UPOFvEJkkdjyzbwqSyU3Ph5SQIAnL3n2yk3xfszC1+JjrAufjfOG4tXvhFIvFsTkxyNvMEXYMahIXQauAZNYyShXTaAo2CLwLHTBaBSZGYHnsrPz7njx9hurfWMFfHJcpAk0YDRXSzdKMyMOC4zJbQNtomQEJjEaGNaNwXK77O1CcnD+J0qL3GyUd2MhwcOwzIVhG2AnwBlk+jgq40EJJAyf2j6Wrg6P2pHsAY5nfVw10L9hp2+O27Cc+INL4g/Kji9YC0mA73ZucA2TcMZItYNYdFw2+4577sXXJYh/QoZ2BLkI3jJizvJnAYPBkrb6BJG3r/8Jm+CXHut1UFo8g+YMSI5KsYTJUBJGpdxPAXqadxKigaa2icI+M3C2CDhBuyEKlE2QXJpRLBJ+s+BUM4+BWZHcF4XSQHB6jJVgaxAHkNgP6cDsAaHoU4daZuzVFc3YoP8oTFoAIuQ4IfnghBfcCL9EYnjuJIaP4R53w0l1XB2yB6DAaYls4f3Yxgwe0f4MkPASNAVo2pgzqwTWkID2XwS7w1Dxj8mYA0KNve3onC7A2EgA/TzREOHn9fENbOwyn9FKPVoWeSwQj8HfNCJ4/t6QdnzLm2AefDy+refLRBHbbgaTbW9TzcNxfzfmckK0kmsDsArcaWdn1itW/IGfpO9HlWH2zaxoMPUWsZQv3wBQHf6Lv5pIEZDq+SYsZ2Dn5NGTe4EWIvtGE8cr+e1I//WN2nqMQOO08DBYJljDnQ194d0KdsHVxejmjfvC6yEJOK+JpiOwCNx6cTr9u9I2WJTRYPx5w/8DDM3TVXxHRglgMlqSDIuHTKK62Cp0CWRrXcMpdtL+gSGhl2w/PuxuoFDsR2zVZ+AO+VSs2EkhNRYA84SEBUw3JvEVKyIC7NPC56bLcGO7sZfd7HkNgGmDBeI7VruAa1EXOAVKh0T86LoU/kaV60jalAAQeK6O4mBPcBCrCokf820GrQCm4Ttx++dvcXscC6YafoijjLAYRPspJ8hSG3dTjDOBeHHDTbyeUWPgDBbD3B0Q3wA8ZrNqWftseWE6dTsPBJXtRxlAvwAMICcqZkoYkGG2AsSxmu4aZMTmv9aB4OfmFo1EyO9OJ4qESLCbF8YEVce+PAluhYIZuoZG4HZRorEF/jVDYLPk0XzwTxhhUAXmB02C3D89XAuHMycXSvA9y5Sw6gInhAe/p7HCl/JS4FFtggztktB/AptNXHWzSateUCjMy+qF7eomDMoapuzx+WvfWBCeAv7Fam3LVAx28340DgHHwKMiIVhDcj0WLBkSgY+jBYDo3RGDn4Xm6V9wQ2an8wvwBniljKVChcKC35v5SVu3tS5uAlFRkyEeRtGeItnDqWAYWVmkAQfAAqaTScH0wZxOImX3f83vX7PAmiy5yTzzGGBNqOIwffbIgIH18eGyfXxw6slOoXLqkDUjG3CE5bWgrV7cBgwBk3CqBsB+G6FdWM0yja1g/bi6jyTdnsLuam6sS65iMHdaUJCHXTM2zxYBvns1StYDNgPrzlo3zLUSYGQAWV4IMz5VfRIRYwgCn7ss7XvrBc5HUssntMWOAq0hl6kcHsLNfcE0BlQIMZw5vepNSx+3XmBBcFcsIYAyvBf8AaDFxeFnEHdADhg3LaHxYQEbW7WlkrgmTCFwzHoXXayShv94B8YcMAqnX8bNuKGS4QzjgETZM2TxznBtKMRxH0jiQsqaqvxYKJhBTzXAM3d5vuvTm2MSADPPLaNLINi/0qrjROtYV+QDwXvBgr+KV0P3uqmG28PN7VyQbJDMs06Jx333H/OppnFwj4IhEzCuOPttGgcCto2hwwywBxiBEkP47nwBef7CjQBR1i5suBv2GNeL5UwfYjKQPagKzM34T8f5/6IV1WhFwHxC567cjROAp81FYceNrGctqbUtU8oUMUlIi6RJNHJn45YVOwFKek+oEqg6LxhrxnoBbj4BI9wGfgsAWJkb+nC0EPcJP+XGcc8TDCq+OzU2XMV6KpzrO78LeIO/46o+GzimbSA9+GiNbqkWlCDxCvzIZxLEFDL4Y7Q7WY9T/9gmMNz14vKQTUOC/R4SfYvFZJrgG+DcgnGPp089Ouaza05PTqWwD/GXNgftA7QUNmjuzjf3ApMfJrQKH2Q1yjBB1HG1AAt8ApAF6WwuV+VqiCQOMt3Wsl4IM2YasLinEt1w0MgwNGD2jrJanoL/4uYxPM2gOa5am4ijrGABHP2QbEbo+pMgvwjT0nhZ4oBBX+YVSzlFBL4bRGKNFt6Nx8DrsaqfagTRfDAkZcYrAiE2rNBYSbdcZ/MAp7aiHKgBCvyOJN7IS2JnR+bF598YyPP3Yyzj0p6itM2U6LY++jGtVbnFhQ6/WAx1FmfAreKX8FSIPJAPH4wsolNsdsptXL0ito8xHBSuNVygCa/5ZLZxm+oC15gBxQOj44lrGrZG0+HbKIeFwVZeJPgaLhzXXrmG8owi40r6T23UHCwGm4kfBmthz1dCRLll84y4tdR4g771QjTiMGwICNAHb6SOXZ3PYz0MW/HuyRMFt8zNw7zUzENb5OgjBsMutdxXDvyZwjRSiW9IRllCNYsuMuvgFnxLMKccBbSaF82uRRMoz87vGslyxcsQPmz5lf/gyp9U4T5GhPnhIUfRQPBA1aCmA1sI47FGLS7rKeV2sKH7fa96ImJrYmJZzI0MfRlbhG2uB9pbVb0DT/gnzwuAWJYAgmvG94CSgZMbw3/p1Rus0ahRt6TLgB+Pe1JuJ/QJXMeJBhTAVM73K8YA6CmcrxUKLNU7LlTJCC/+Y8RlBSWr8fR9amhkLF9mN5tZmo+Lsif3wMaBLvq9sQ+SBEs3EiwbfQFBpaaLTK+OAv6EKMzxQ4pQ6FgGm9JNG4b19pPPCTxDDwdnSkEuSK+GoMFkkGc4K0ow9fzsCT56dSxQz5sP7S4OF7IQCN86tAkmkibAyoqoVwL7PkBdK+zSH56IEJjf+6Xt8cNHu2IwrIjBzRo3XW3VaT6yuwvS1u/6GAYy+lQADNaeQK7GbT1JBjxOQ2J4RTP3RjZmMhzN7WJqke2znRePgF3poKSTzAiYGUx4gYrjlmEzxUz3fZIZ2IcFtzcOHmFwgF38EUzgsRrj2hbLm9055RjZXJClogY+IrqRDA0/J1djCqmY9Igv62DEtUFFUtZpxX1he5E2cMlGs7JhrLMq5kkf1NyQAuAXexh+9wdT6fe5P9CuzQa4urnHcy0k4sFUmk0YXWJ2C3Qfi1W2EgGpL6vIrixsiBZKWf1r2q5BR9i04L1cQAWU5DPnhGqbkB/LnekgC6wZ0mAgsIn1e9oGWTr2/bVsA6uFbEA+wf79snr06PQYH8AXGAMFhkI0C7JeXwNxV/fBWr+6DeAuGM/0f3fvoQ+NF11rngQkLzQAtQ1LdKwSvhciya1ldDf0DF7EEoWX/UZVP1Q1NeOErcOb8VzPpe+LUjtwDY4Mn/fwBACFZXY54JutPskCVDzrfeOndDWYCEtuLQVip5J0nR/hO62QPmo6m1FrwTSouNqq0JEcyzcrrP/4IGBHiRJWY684+JMMv6qLg5EaM+Fn9uGxlvSZkAV7PZZo3ayZGS+zkPhyWwegN8+NIQdWs7a8/4rAhnlqUZHPWMOvFNUYJI8MQcUs4UwBGfhNXsmt3WDxnmP7IAQBw10CN9JB/uj4tyAA47E05NTWgFrPgy5Rp1HjqmXfUhIA8QfisP5B48OOPKCwPq4XdwybgPayWOzLsFSFj2+/VAL8AmQeKvYB+s4jRuuz/0vt2PXhoRbQ4sckoY/dAJN+caXNTUxL8/FfcEiME9sQnu+2wLENvTz8LliqDc4GqVj+D+hcH8oNSyqVH7vOfQgAACjPa5L1BT8sqOa2HCClE635TkUBjwKIOHAkKe9ty8GXys2aDjfXyMmwmoSVR3k/SOwY8DPrPO+WAc4b29TF2bpgDMGJWgHrYdxwF9P0mMUU2dRmSWhN7OT9NmT+FbCAkFFr7BfGBpMZ5rXEW/ebJDbswH7yB2Tc6GUwk9oTSAFKejKp/2KtuF0glVWcfBb39FzTUrFkxdaC+i0rsrkYZvLeYv5WPj4HnH9L5bHeS0RyS2/Y9hsMaXibu0cgbX4qZvXxItJsoBp0Tji3asntwRhlq+4RK0lhxpKYxrGsCvd4A5etuLqyMTmwSnm5I8008mE1svX/Y+MEVrC2Yz6nq8BsSIT8Y3rYG+gdSMkKnBSunE80FqyNSiYEOgJUrdYZ7MxYAdPS2Evg4rAdyUrwH3Y69dB4yrJBxMZGDIrGR8OyrGP7tW4ZzjREW5SGtliuijvMG9ydc0JT93lo1mmUbJlduWStMKMCBAcv4taxwyA45G8NEQxW1MYkY+magseqQURjvNZO/NIowtR9GcLd0CzcYS+IWDRd/yusBCVndn599an4SrBh25bovj/bNbSWuu9XpHq9ZjoKZgSbVtXBA4mRvAfGxb1YlVRPylTk+0KeWuMJJLZJM7OH6byhQC47UrCWsN37u+vxF5gBCNStLgExB2YMQ6kWdhs+ctedfUAzjEPj5jFu11lBHFOLAF6R2W2K3uaSwOcEgTQIelvHkwu0z1JdsLRhIgwFnh8bUu6+r5HZGIQfwhqx19oMk6CY6w+uPTBUeiTtZrFyWNyEKXvAtCiNZewm/0DNmBGr2DtoxHRTsOIJe/m9h2FChMDKYZZsADfi1KeNLKqAmzL0+taqgYyuJapouNZgYYr1a2A3ywkfuPRchmuseHwtrERRwFRV1Ic4BUz85jEhgCjiJY594UBRsPyt3q0ymP/ppKR19i0kE22Yd5wFpIz1Z49g7esKxn8acoxJAH8VbBUOCV2yb6JDcLiCevJ8+pJfwoA75LniQXH5D0y9/uBUJJv1ACw0CBY3m6zHkn0+em4sBd+KpWC2i4ne9Z4mrT9YUmEXjXREwUAhbijNZBaVxzxMTXtgeXSX5pifke2YolMd8OCo3fhTTHv9z6pp279iLuzrHH/HXNcf0AV/MRqTFXlQwSkQPtUlI58CYathfrUln2FNHOUfW3RaCDFF11+26DgPKSg6LwquFc23isSwazSchNtt9olxE10DjqnZBsMwjY3F/oxYbfHHvE2FA3zmXNv+sS2YyKwVxmLvA1At2Gi7ANoQclA0T1retKyGBp4mWYP1S6fI718jX/c/Il8n8MUTwx55YH7ZFoKfR4cK/yFH1WpWxNaaGRNqpWh4UDC5wivIeg4bSSBX23qgZV82FBXr80CqDJ4DThsM8mhrxCfl3U38d0PaIQx8RshmO9SPX5isYlnMzD0zDbMqaeK6+cxbLvL9KBVKDdZAzXHLlqEM3ht0ty2DMqdeMpyqk4pqd8lvegvGBS/MLtmcE23ZqKJO7ALmCkuewNSHPmIXDG0GS+E7hJe7FYHyGhSixNc+E1B0QwYuYPlIZnzSr7PLe0Op+Iw4LU0CWQARQz47hDP9Tn1cGdbrWNo2DTXj466+0rIqDdGtZq3Q1CVoj5iD6vIDOQ2iY3Ztr21YHauPxjFvArwfvCvXH3TXA4rIfVkkFozl1oCA4L/Rj2iB25z/Ar9AM/irbHwas/NBavOF87a8hssB+BC/Em3twh+w9gamEOT32PljQKBGRX7Rz4UGeEFSGbEf12s9CtqORbKwlguOEE9CKlgdWHX8sF/E3mDRsJhPBrT1hTvjM3PG+d/7KjHJNSa4HraULI3GVtURbmHKqwuo1Xg+OtdzM8qyNpQNeVVn0ynlT++8TphKhi6W5jXwyWy4kacFHp6UaDdqYCU8t24xiFH9/ygeu/5Uj/1r8RibAWrpkZVILpObyRfNQgTQ+LSCZQVrBvAz+VlTYHphxEAfluc1+BSGnIezd8NgK2BzZJtNQK59wyAEz930xYt9lMRzX1gIgeel1t+YLa5nV96HCQHJVVMVrWMJwKrmy9722A4Qh9JqTkD6irGcoT7WpKYLT/Ta5dGKmSXuLxjVscmUt2jN0ZuRYN2Wlkb7NMoC9MGtKzgZNUUbH/T7mtDidofnXbBQSXHp00YlzGOCOZVoyKmDCiwHrmjkr8TVuHmpJsUklxgy5UgfZPZiaRxhsBCQGq2PzVJKlDuYtQyWBSFar/UUAc/XM2hTJr+th9aMRHuxpmX3qZnGF9R8AjU4lfCsbX+MhFZDZBNysbl9aF5E2JA7+3jeCymcRgqg7EZHv32CLl8GeW2LpupdLPGExLKkAB8e5GQ7B8alwAwHNw/IqBd8l92quD6D9NHwKReEImm4AWx8uuGHttJ8LEjaQVbft0Uys23sq1kXkL/fg/oixicnbecAsBDAUSf8zkgm1hRzF045lrwdx1ds0UP8YtWVTy3pBQIt8TO7NTRG1rNAsUxdDrzcbRPyzKtsiVAW/dsLDBKV4exDYMyF73mZJck2I4B38BEs7C0yNPUYc3qgO59RX9sqAKZ27L33a1lSaRaJchubTx039mge5Qc23sH2lGLhEOv8VuTZYvZqItwacPwSUiNyhC1Z18Tn2DnMvdV0sRnNQGp+DiOXa0wUZMKj0Y9bMiyxsrjdsrvXHOh4ueobOrCxPoCdFptTGdr6ddBh0Ies5fYpIJcGc2tt9iu0Ew6xfMemFWvIl4D6GL3WuJ+VLjZlGb7HAQLvHJZg5Iqv0EWA0qjWt5dqcgO1tVkZWUd9Dd3VdmBQt3H+shVYqjar9W8YhmHWx2ixWQUzAE5a4PYhBaeSKqa6n/BYbWG750on03Zf0fIJiy2TPUe8TmCGGYqhKhZgMVtpYPu4bJiQt4RjsU/vA972fxTaXv9Rafv/sW34MmRsHyGLx9UqNHT9Z+fwsHnuF31bt6uA8OIfV+ct/MEyPRfb0zHAXLT/oO9gN+3iMFhvwrJEy2cxkzaecCFbfRv69IIdhu2NxQLMx5DGAMEA99AM+DB+9gvWUiJKc96/br+sW8k44z7QF1YMJjTvEy7HsrveJjP/JcLzWAmDRsFfz++7IFGfoVQroiAjcYE8DXalpC2Csx5++wSIX0Ne7VfA7gELba65mwFuw3HcTW8HFsK6cSWGqLqEDZJXgEk8W7dUCdh0wcLGI+iVieVgz/+dwbbbkR7vCRvinYXZkB8UCaedRrVFDWponS02ke/dl/0yRsirpV4bY2/hxBJuWUSUDQwPAbzVaRh+7ESLJ6gBpux2Viisowe0HyP2sKyn4FYGqdcasA+bN7EaTgpa9wFzpgVehxKgcY5IKdPQncbufS+8zZ4DxflsGYkNDJ4fC1Z0hAc8GnuL2KSn2x/C6gY7J+2GGPbKBLvlV75uVABy8lksOn6916v+Z33/A/BvE5nIDq8ARtrxCiPBSPGKmr7rAddZ0bmxHEipPXWuJpd7oNaOOfhgkDxrasYFrfXCNGFlW0mm/zDzD3QVw/YZjmRRn4Cc2voWNoth6Of+NQOWfOYZfHhJBM3++YD2bcygEm3OQzG8xHNqR+X9SDd83KYBKwTgo1i/KkRmZ0EIwAr7PQBR3AjI1olCLK69PKlcUHrJxwOFe8zlAz2WtVw1dchLLa8UBPFq4MsG/0Q1Hawgo1oWMgCqbT0e1wB/maOwzDtsHFUWgIYAasmr6Q/wXuxMMcqEaTV8ZVoFsX81lU7ZwWR8FyQ/CfNxUepAEMgMG19MAf66Gh1X0ZOdQmzUgxWz9hhJCRGoYkOxpWLXXylwRJ5d+nRneLdTQh9NQ/34RznFYnj8fltl5IY+7LZNacJEC+K4lTItrQ5QOSA56s37C24ajwGpMlH2nkA/fNNi5hwaAla4XwNMwj1b1U4KutsXZPAqWU7HXgYY70JCnArC7Wg7T0M5LAJzItV4b8u/LMM3jM4NtesBHfAuFsWqUjvRoEJHxS0FBYx9dtxADgSRp0q9dZ7o8/rAlZK/D11dyFGzvkFEhoqjtqNYzWO/82O90GcWBdwYlRo9XvhFBPDTG97RT9jKUt6rmsZ4HfXxyZym7mcnHnUbSzFb6JCFyTahfP2MTsBoWGFwP6bzWUYsRMpXiAY6g52WOLBwKm8Szxg3UE3nzRV8Of4Br4JLGdniC2vOoC3V9MTCG1pVj+n1Q4BG5olxqI/Q6XQI8whaANA7/Pe2m80Qv3kAxBsbYNEPbrOelA+P0TCuQNIPyGc/muMIkPrbaiYbfjfGI9oiA3+3wBUaYHnFsHMIE8VzgJavaS/bwy2ejlPE1/4AQJRh7Bfwha8+WMWQxD7lXFXGH+etXcKyqS6zxMu5M6jRBBpZQwUcYsFYe6gsaIUFwzzCubl0BJ2xbzZVcBuWkNeS9/2t184zFnsDmZxFdXy5YfzebeEvZrfuYQwVl9cbpiGBVLatRDc3/EnjITa7TCS/XzvoKTBd0dTfdwpKC2Bu2v8ui73lQGiFgYx2OwJILKsrhtxCXG91P58inW298/OYMOvowjy9owBxJKTkOU79wMMdDj693Tza3ma6Ma1OLrKBD7R93dgt6M7G0llznWwmtjHe4LesCHQdHSXw2GlibHMEhxjdNhbqeFIzWBLShf3FMnPjwl0nFySnBe35K5NBnFzP8rTPFubnnu/zvjhha5XtXGfNZhHzXyZOEzjR4SJTE/Waa7tl8CVbINMrN1YgJFYkGsfO7jaWEI3pn7qf9HgSv2410W1gBbnvG6sI+xEb6OpYkSFkCkV7Z6eLwNLE8iwOCynQQ6MmVwb4voZ6TOgCyuylmRgDnKkRhySvRy5ltnbOmxyAhSZzPpiy+Yb3tHzf1zLRbQugLX+wAT/BCTXNOpOO7wEQ4K6Me6rzaRe0Fqjx3MWSed55hrxg2EynBbuz8DbWvADGerPiJuJNDAMCB4J2LX/2dDjQ59TgfuL19iY5dHSUymiYWFNG96/37n80iuHtjtGIAH1MnQS9cEd77CYgwrVjiJLJDJ3stsZI0rrsFbhtUkYFEW3rVoHmVgBY8dCkjbFc7OxfVZy/OQkni62ESsDs93N4SwLJvrbaTieBJONt9gIiKWh0e1IfV2rlVRhLOVVPv84NPhp/6aQWjQmeFKQ0kMVusUoQBTogBfd4ykYLLDZdyQ5Ug5/acNHhqVPFIhv3CXdj45B8PvSONmBncaFJzCWxdXIMYABu066NtRBoqNGAJ+kztj87pwL/bp1RgCotR3lZ14CwWb8hPME85e3EmBcrA/TTWRetnLH1NR0NBw4E8xviRdKxw20VEKAkrf2K+tVCXGaat+O5IBEFFcFzLW7dl9/cVDdjOHlLe7C5GGLrevopP7gdGzJtk37GGRECWXgjTD6GL13FVmK31tAn2gQJymeI47st5TRmltg4JHVtBxc8wakNnyXBqi8cyhkkWEg7xIzLNSgjWA9Hi1R1cJF9wTybT27IDwgIeXtWcRgmdNpqEcf6sJRgK1h2gTdIBwvbjLQ5awss1Es+UUNYrEO37FQHsuDpwHW2SB/PDLYAbiTbl77Oo902y3zIg2itmdUTMcL5ME8fptzJBcVtgxEqtb84AA4Vur4aavUYtrsAldnOic+msR0dFhidUKP7zMPqJavoP0fQsAJW/2J/MW0sAz/IFSr8PDCDC9Xs+qY9gZ88+umF0dsfEbBJGF9VwZntkfQ02zt5TMC1Xi3bFsRj9nZlSK79QwPlRKBRnXfBFHNewbY3vDr0tUd7YYpb70ABy3U70AxmODXDcWQQWzsdxI7/eo1324vOe7NQ2KEn7VgreL+Z5pzN6tmUO/cSHxdLEDeWcl34H+SI+3T4Y26nUveUDnUWqxu0wivBo74hOsx47GGF2iP05WI4akef5HBZxm87fKmpWto9bPO25L3+89vNptVlzxrXgEk0sxKntKB1LgJ8Oc2C968UES+IWf63RKWl3W/+bnu8+df82mnLf6LVgq+VJzL84oizcdk6glwg+dnOXJGINffsoyMImiL1rl8hGaYTWBsWsAC7z2OXU/AG+bxzAUOi7EDQzwgN8o+AInVQO1vEEaSdv2xYCun78DXQYShsbmAfVfg5LUxgpXrFypq2j4V+x0lwg/5tWWH9nRs5TWwD7xJOMduommSYoFaW6y1nXMJu47nXZXToxvpZ4G2GREoDHKyOFpo6UignvnJZCHCj/2B68W8Gt9oSdCdQuR27l4b0BQaZXge74qbwCkbX3nDcB0p+O5cm2UKIHWKXDFl+0V5FSyD5+cstXHbvgK0f3F5crtCdx+mHteR/zwVQxupCv7vzoqrBbUeutde2zGTm5kYHv3CtNVMMTk2wtGynZYjqMUyBXmLPKwC+IO8OXwTRd/MJ+D9vFQ+Aa9X0VKcN2Q5rlaJGFKAW0BM9MiABCzVAoHbxS3uxPMCf6mSUavlgtIQ3JC57WzN6RusuhSeciCF7EQ+rUAJHPT0avBNkxaODmkKHrYFDrERbOT/bQD128aoWLb2nM60YbAxV3ZvgNPiS9VOOLHLaqI107JcNMGhYbRZQsGyAUdcinjaP+/smfnC9h5E2OQn4GkY8tyoKMgPu6Jr3bCaKBz8BnYNnJNLwVsy4PcfwrWIFGCaf3RgYLaMyUC1rYsxYs+AYSnOjn8kAfL6d6HhGsEqRJiMQBlkcV2mO4x0QR2sDS8djsrHBsr1t4zDGCrM9uklv1AJKgqSjHLVaKYZJX9do1kVDOcwalyXyPzXlYmZ8L8je7IvdS2fok7MMPyTjq7/xJOsk5lD7yzb7iFW1/sXdG7q5Gk3OBkdcJkeWWJaTyv20fULMkvSbH6czD0OKW/r12Ab2mHEw2YHM3MDsM0nykZBA5U2DnPr6AUK5j9NBNQqk99QspYMN3uuMJLJMC1+3AKyrLuFMdRYUi47gvg6LSkFwhUAbWoVgO2TFuDlIu9p+VS/H9QAw2mkh49NbcyZdW/a12PkOynHeEOtnK3R9F7s+v+fW044z0ANw8X0G6xw8Vn7dQEILJ9ZAP9bD7WEWraAdITnRCtB2hhpja0CWBmF7P77HUNeVeWSEzHGcGM77vU19DAnoM7kJm5lydkihbQNY0Yp1uC1TMyXjCC27flGvKyscyWCMsxb1/I4hKvVzRCWWPVYsrOXeOGPL/0CWBp+es8UGpV825FcNnW303sg/SmFmY5woLE9jK8Gwo+o5TWrOCILmdNi4Y7hZQ7bxFLkHAfvOJyzRJGaQkzP6BY5qI3KRKqeXxQVjuuH6aNwvd433MOJuNxtu1kaFSz8P/r4dvphxJMuopR1HloJaBBHgI69zBTJa/RocgXg/5txsVXXIw8bvgEYw1s1mbXwZdqJP61rsJzIZaxm7dNMOxX4bF8GR328cDrW0714HUbzZehlhKtO2fOms5ZCoyNxmzVxIfPN0WM0+oxyBC4DGDx+CVXgzwnU7GMbClwsvW78X6UWO1oYSYa7tH7GVF1zel8rKpiFAwtcxohUPzTsvUFl8D6ieZ7kAVo5qzckWgwmKDtri5jzA042XXzBLsDhoBbv1XvOQoJkmbMOYP7napgcaOdlza/HW45RMpM5+Vqedngockb1DUo39tM8J17aY5xf/ZdIeSTCiA/L/zkAqwOKsNqre+KqnWUERAazzIAvLXPdnRe+EySUT4raiRPs5F6oj4myXMSE121FLeObo2HDYg1gTCb0ttsA1QIX6ApQuW7yXI4Sh9Okpf8W8nte5NX8w179BrvYca/MPdFX/QlcDum9gyAmBppIHrtjhtAZZNkAPe771i6DvbEsERJmVdKRD6E48v4+d06HZTVGXdVGv2fa/SsOuibN8z5g1e7nxjeAMPOh23IhpU7VkgLaj7dudVXJCZR/1wfYEHJ5dgAgrKnIGn/La0/L8nvDc0+sLVIc+WBTQX70U9oQXJDtt8XwooOUROcJ1biDcbTzbNrYtEjohFWuVHkNQJnKs+mmsDICtOumdB1L2IcWQKZ4ss8XYhMf4UYW8lG3/TnfWLfpsyw8+z6/uE3Tbj+ON8eeQosciOUFO6M0qiFtwY7/K5RThFCGHwSe1SN9IKU86nAb6NIcGBFxNfD4HTbJl+30sSEQnCzqejAQjaReAxqmR8sxlUs1ckl4SymieIJ7x486bTPr8ESJIB4Z5Q6+xug8gCISyjUPaFwBzRQrsmLlRCvs0xUi31K7Y9yVQjF/v5tg0Hxbhd1bIuRkZ2PBA1zsS15dRtWEzpKXa4022b02jqNZHvLMjWtzHCxeJ4TlhiWFNqNU45R3mYS84Id5FO4zhsCjKyVHD6BoQ5EymtSaQdfi0S440qK8juU5F4apxnDjlSJdDmuuvqPDu6wADnjYZKIqO5JoO6X4caupIgD8C9ZqQL4Ap3W4bBoYucNqUJJ6Jbg560LxCGIZZerzuQWzGZE4m572deob1tAY65Q4Gg1p82N7LBGR0NLrjGwBi3VHSwH3hDZ84GkLWmo3WOeoowYCW8FvhI4MDCPUuXb1EPA9c5kPmn+zAU1zRqqYd9u8ZP8TAJl5TACDvG1TTHkccYhp4jIJEpHtcLqwRNGdVFTuvjOk/JXX4rw6JlbFFdN4Hw95OpHKE0XdGukacU/m0oPMaBt3aMjFvzJCn22dfP1gY7tS5GNFBYzr1yIOCGQRSPLxFDQiuPRKQnsuhC68W1ckQ2XHDxQunerLPdoTcLUm1HNFkBS9vrsZ9nWio73U6fO88moUzThnAMXagk2M/9CCCkYXqW632WOPp+ER7gqu1EYatsFYOZ2Th+USjfr4X/Bf5wHEmYTlZDMiCAo5m72QWFuH/8c2n3gpZ63bZbbYAU2Y5zTAFDUXJ6i2w3VCM8wa4TYsBrZ2FcW67LuxnTe/JeKlMzkg+IXOcrRO446XU2kOLXjwihymxg6E1sT/3iH+r2Nlmv+4BrsPcdnJa2XJI1IlJhtNNjxnKTovFGHTbZ4P1kwv52AZNXjshq4PlSzQq0fjcz426q63VuAk4LE4KeMw+20yIFwmOe3+7E3RfBwTC4Iq94g7gsKYy3Oj2IyPNxlaNMGG5z2SGcVmwKmKswC1gexFiKSFOfrdaZjv2vziJsThrA3NuJhgCqwqdyYsdY1Xv68O4by2W7bf41WGIf33d1o9u9knmOzxcw1IJW/Ht/fgJdhoW2uFhY+hXfKsJdvAjVtI5Pg6ePFQmJ8fOnFgJLuMFHsA10DpuXTPhh+KVN7ibj5zXdLKlQBixeRZooJu+BfS1XzyjnhVmk+25Og3pvPo+gxtgbxF9s0XsmxfEBir37dMAGI8+D+O6AFunjDtZ8cRQnfNx+kj+DC2vc3m1Ux2AhNyXKm/Py59QVThREisCd16Qn3eaH+0ytj+b0JIF4lOJCia3Dt4KwOPoDDCcWrYI237k4uiXAWr57UM7+4BUwyw/G2S+4semf+bx8A5o/1+Y5p/xIj24DZBG+EzjOcjoEConx8YPcXNmdHa6sMcamH2Hr14Jpt7XXe3zwgQYGWx2BuN3k9XP2BCUAb8PtzGWff9JTXEjLEWTPC8PMrjKjVOsp5r3O0fb4AvQDmdUHUdTgqGVDIp1JEg2vI+S32deuCGzbAsjS8gd7WUNTLQ8PTiTAueABQoddOfkR9YqTVUbkTLQ+56heLwSwrMAWpbZY6ouWwf/dP3bQ4Z9iHZnRr2akx+i/Pav2JZBQfxnMaBkuz/ExPHYDna47lPek4MjMVcsFjFGXU5uZ3CVAqgOfGYSg0Pjz6jM5NwWS/VYWGxDwx3ZVnbs8DQrihvfdjcsKwijSfXgHA18e2EVN0Z5naTdmXvlcRWq0Oe5NReYyhglhP1Jfxq9bXrbwRCAuS/QTzsNy/Cq9tjvpOBbF/ac41oQW2DXuj72XzfX2jw4yjM0Ku44ciFkGqfPSp5epe5gAYc8Iq6AL1QU8V2yQmzYC6wJqMN6HajUPEKiOHtECA8ZR72sKvisNnX4ka1QkIjq+PeTadApo3q2wYoFucfEOgtqQCHvOUHhhjgGKxdzd9BXtPvExH9q94T64i/q65kR+43NthvAqAP0bOjLZxCDB4fY6mKg+Yho/PX62uHhkKZgNqZPjLk1Ly2Jl8AyoNo/76vnfZ4C5EyiXBBWntpZKk6t2x5IUng9VqmdGe2ovEn0aDscLn1eT8aNhDWfW55Vf93787MRz96RyALiHVC21xkdK1lkL9QazYy5U7NClWKYhEIvb24MHLuez7qBbV3co3lBNXGFn7p4EPRvmBfQq2aDv68TgrZNMhXExne912nLHqjf6etTiIkVEr998wk4qPdmV04fvCd77Pds2EpWZGPdbfKqUoxHClQtGrb/Lt+OkLft/xS3wy4NxIKYnmJVmxWhzTahF1s9HQTTFu5omqXVANwn5HAKPl+75+3ddrdvUKHjMI/a4tFOd7slvBaQ4BeabTnlwlZ+SPYKbH52Cu44o2shZCuCgDE2uDi7iYx3OCgAMOnFHdvjuEibSNL63gtobk4Ph6wltM8k8DyOrRsOYZsWFKLpRSCEYY2I+u24J5FXNZWr4wEIX9/Rx4XYwiUNHXoaj8e3FNb5wRliwG/WDovnkDGkYm20t1Q98+2JA87kXulyur8TKlHRaemMpwrkM4YB4/0BJzzX5zeowXbsYeniGW2QpSx23RtleOe1XkfSRgc7hMCqCbMQgHLbXlJKgophZ4Dw0ULedkbFOGwBc2nIwdxdEDBcbJpdKNUUsIEgS3t+c0udseoIE9sfTm7h415wW7i8A+y2dZ4N8ccrpXbZcfOc/LIpb3h9jN12kIIoQ+qAMYC1E24pZkAe8z3oo1HJGCw1Av9ZiHedY1a6vPzP+u9bOXKVbieHKV4WuhYptB0abTtrUezT04lOmdB74yVhHOdWW7WFCU2ATLlD6cgTDKX+psu9mOoP/JNxstGxe47a8ZCapUJctmh6rAw2E/9arLl1E9wDayrin03Yu1WjatqussyTJUcFtMdR0s4jvNhZtHGdmZ3BFg7rwwDz7IXjwjFeHm00T42Ps4E8Z8JmRyhJqadJQ93u+3rWCTQPdwNHNTDVT4foO2CyOAl6ephSlGU567lZFxVs12AR2Wot+2Om/TpTKO/qUv86wVBmIIux0WG46TFRaB1ralbjWkxj8MMaLFQ0IWInnzfB2c15v3tg1M+hD98bDBvjmJ4PJGZQa5xxx7cHSTUj/fYRWq5vpBiPjPAOSz0doPSrDYOEOO/U1hOjFO+bnT7ZbqspUKIv2wACdkhORZwfKKR6qImzmxMCiUUyGxAdVYa2YIXMwxcDE8qN7hq63c5JRybkUzBa75l/jvg39OLk9wu6aGvecW+nyA5CtbSQ+rcRs4zE+mHLF5Kzeu43eQqUCN8YoVPl2tObcUiMgNOvMHSJL+zDaXZonPk1CEp66knnBFzJPLWrngdgqJBPxagCD/Zbr9a5kxY1CI4ou898ilNDGjxQJPQDvodJF70lptyRAWnfxk6R6Q5W/HL9LnR9/iMIdGJACAPWqoamaKNkQTrKaosa81JQLZKEcee9kmfJWLEZLfT+TieIdVFWzsZpoMhDfbCbllw5oxYDPsSHYi9TL1ieDqSztjE9ufeSL0zqA/ILuj6bWgZGR6qWrIPg6aDarN8QHuEoJRhdaHsqa27PLXvknMAaA40Ww/XbU2xeh//eBunhrttjxRyct1lhXHDyHBNUyQiBg+BYep78QI0Ig8Ro4zvPyUEO5656sXd7jk35DUyxHeCz2AtCm5za64Q4C25DBHhaeNORJ0P1qGpa6XflG6N4qmw+J8FBrx3NbbnfZq1ZR26voG74n9CKxrC9tmRg/JtzLr5i5Gstc7XWxT8gLCOKxTbvR2RnjPwxPpxYQ2yJSme+n4+1vtkzWGDwOVSgKgShedaToQRlH9dl2UE2iJzQoCDnHC+AAjjgjJ5q5K6c4Ti5XjZEvWyaXMagshlbFBIE/1j1I7/DEVi5Y1t0NucUrQyCGJ3kyWfUPTwatvQ6uRwU1E88mdsRAn328iFkJkysgnQUOpupWEQ7eL7dbQOw16864s1zagw4L+kQpFDu4vFCniZnLIClCezQKVaVawInz/BXZ3VvG1qEGm7T1QwWeULdnKt3DCorYKU6dqQ6/AiqjkPLrF49R4JhfVbcudrTAMayZtQ6XJC/E5YcZXQiojs8nmrn7KX3M4UpvRjh5Fusyw+ngBFSYht8f9gfwEDLIJ1rTLywByZZkBzwvs0RBt3EGDbosU7C8bNYBNd84Ks8PsSjNZy2zm29GBGufp1s8bAoA4kzPFWaysyThHPGClDRk8SA62hnRk9PlgVqbcv0a4XK/Jx0eNm44TiuLX522m942q+3PaZ82txBFbPXP5wBfMsyTSibXQbwmc94JZt6nZaC5HvOQHlo02nL62dI3PC4N5w0oGdX3A4G67MnzUkO+xx2hln1WLI0rnBOCoNLRQ/CGQIVO3HekztkHaYD+J3aMjFmA3hwZlAYgtA7LPCtibERLzb4LTJHtsB8ksdJFNtAMFNPcjyks5ofQ96vlOtZnsgIsj8j/Z3H6ASkL1w2xqKe9QYu5BQfx23ZYnHasoJJtMPIRm6mDR729HEq5bIO2nxAPoyMC/FkMIhmZw7s0BzL+555TNWQo4fTRQ86WPNUIf2Z0nk7LCN4xIUH9xQDateeDsktL6s/HNVRdzeEhp3hVu01Hp5bhY23muzJy3MY57S0wzZP5w5GzyFYlzs8g3PIVyjR4rozjYslNVKHWO5iJY5nu1gN1+q6MeE2+kRMHJQFvXBI0bUMFmDTka0zMhGQFkTyPnpdTqQ8SGHd/VR22GzwOa4oOYvYeeae6KFf666DZRnJDp6+z6jNIIKcjvALzpl6wzltop2aud9eOW1tVbjUymYXPkCEB26e8RUSxYkjRUkbFMADYh57753GGG1tBbEMbX2xyFZbfqpcgAHFImZHHH0oqR1L9z6F+/AGB+s56t1Gx+mIaUefOUCwW1y+PI7w9NHN4ngPlCNeHkdSD/3gdbf+9I3SzrpPhhap8aCqZlU+ttaZ+8HTvWxpgHXgoTAXtbd1GWjDa9rBcf/ENEqXPbPTIy4KFhRcASLxwA0nkX73GW93St5/YQ3MYH0vs2T2ExjP88whWOXDbeMsorWNw7IOz8vqp5/KqbX6bUuQS3vOYIIYMYQQv3lqAfcZrDx6M0VScVW46rc/ZyBytOign45gqY0NLPEU48jP/wrtXH+GknsMDp7YUrSaIlT07t3o8un8cpSa4QXMHHjDVj9rbKLFiibHv+xRl5YB2CsjR+rZMxdsrDK/GL8A6oue5cLeOBHNcsSS2Y7+p4MFoDPltA3iB089ZememNuroZ3lUaHVpt49PYgSFFcc5G0Qsh75nuaKp4OHthP5rPmMlydzIXrG8+w9xdjZnDzfZd3mi6H/npSq5+94ooiHWzpS0XnP2NzPgRAf6la+6y8kM/5CMgfHWIDwKyVzlvkvnnOHXzynnuH0978HVe7rn9EYlD++psyqQ1TO+aVOn1+xSxzOEZLTRsuvczEM7ec55lCVxIp96zLrHj7Pojv4yB7TV3F1TTzO53SMnfl86D6E8PUkSy7WpqdP4n2/LwlLLmsmm8OccH7WfTyg4Ch3Aw1ZJuUsrTPbVYJ+vLxj4c5hqkanrRIK1g16hHM/w5mdhwKX5FqQaZxrxBl1iGA1+JcfKy0CfnIlbOTJvVuDlBzpz2dgRqLdGk1HBZuRFjuUTj9k8+xyzU/SxJMIw+N5MvaNoxeW96OgzoBxWsXAHaV+YkZn4JZ9/WeQFJKE3A7n7Zp65387JJwUYebEY6Q8b9BhMDAOWC0O0iblV3ThSS7rF3xo5ppa8jzNMwH4FTJwN2s7DL9jxz2a53mswDWf9a0BzYLUOAvFXlgHUj9OfOMeNU/412CnuyUFZ/w2vvhGb9kW+5MfPs+DPpw7fHnInlp1mk3OmWSjfew2Wr+cgWzNn8b1HAN8e2hUt+re9s9uTGGLvFJ+rtuKLnOqCL4xAE/xga16IHFyjDA2UuFpwcxitA34nOx+DgTrSBn++Mz5uiDtPWQHazhcz5EsjzoKf0nLaTz2miUHqjs6SR+sKubpmSUOzRBo35YaXFHeJbN2loeeOXjSQreox/ObDorGKtnl8XpSlXbAVlokyqOFHM63rMm7nLBkmsJZfvbtOmMSxXq/vE+ZRWzjS9FYij73MTZg7Kw6LZLn/p2Fh5pfL4JajPLApb+TpHwOYwuhOtvfekhr3x9dwjD4b0TR5k5n9+NGqjVZ55yaOX4HzD/3GXK+bOv3jCApb3AY6qqOG/t7XT9mFLO2cVanILOC2Bwl57HGjrEzeP4vhf2W9W/nb4C1HDjsrACE3d5I+wYdtr889hmve8ELoQvFGRO2347Xkeae6HU7MSKYP273GaFZip3xt0NkjWpYWGso2uOOTdPbpAUcFTo7IKU6CEjDBuwJHVZoKHhYEDG+E5g/h96b8CwjepimZzf4IJcBGKNBHju4UO37VMAW48JO4vfso3AOmtTH2WqLUajp1M99t2dNlFOlBV+zODx4uKPH51gTDDaEtd+2fzhzEGCzPzMI9n227kj421loHkHSThf7BCyk63fsDjtj5ZETUMdtrarVaqaLfo1M56ST78YXTxzBZgEdJf4g9b95O07QfT2rG/w+PcJUb2FtDVY6Ghw5/v5YI889cPaqA0CtMPMsgTOJyuE3qD+mdk9ct8n3Q6VQaAOgsMHWjWg4I9dDVM4RMc1+wHqq6hzi5xQAy1odiREvJ5tY/LzUh1PThC7cnvXKFtoLak4P2e2OOsrdA+vPzBb2aNjnZKkC7hx2ZHLf8VL26tnK8y1PZwcFlJyWs9jj/kxcnPIgbfdh9n94/X3/xewvsUyskfv4WMTmuCHME8ao130OOvnsgdZeOWgeRdfNeYDFMHsffqVa64Nle8Yqyukh2K9DW9nE5hEen0MuDRnnVD8d54RwRk/FthRT4BKKHXCH7lqfbdvaz1na+eppFA6++dP4Gr/7DJizdQiN9JjNH+Cep+sK9jWcHgCeuEB3E4LkwC5nQjz22+L+TBJ4nnf70Qsw8J0tvd9T61DrluJ6iIQh5y8toB/AH+7r6OTewDHt+XM8Ycft2Ghmg5wzINF8JMd03e/4QO/GUlVrQuArV+m/apfTk8FnfpApkUk9pXHg0WGTPBYevLLQE2enp2z59f48AwZ2cBu3wYy8hx6tH0DYluY2Ccqaz7G9J6HcHbc/rBZyDqP1r8aao0ckFGsfPetQJbVoTos7PC4IoA3S+Cx75jYec27RLoh05MYsicVLVXrl6WI2xiIml+4WxMTv7qTA/RtciF44Py/bS38CuSbI5Q35LcIi56F9juODmWQef/UrRNt7MWUeAMs2VGidXQxVfmhk7f7lf86RE6eCz2rwghSDPns802ate7zMuhek73OGzOsxmq8NwNMOy3h6meuZrXGm0YhXcERwSACVZz7cx5BC3d6rHrrW0KbxWStwzClScx9r2uzGf8+QJhsAvjOkadx/Tgb3BFnPXXCCwWVH9m8QowGtM8EAW6IZW1a7AQO7qbU7O5k8Gv0DLh8vEyztup3272kIVy7LJGn8DaX4zyN2x//21N5/P2H3+n9xxK5HLmEFsQYYkShksm68eXRtDZZ7eGito0FeGKqIxkPC+20bUzr85RyXNJbzj3KxLM+S2OwxyFDk96Rl73CtM8fHV/iyf/v7GTmcOQiPFZH5hBNwyZ4hb57aoQGwhNzywhJfO4LiOu7zTQE1sBGivS7kSKaNupMXjvbZfgA0c1IKbDPFaQtL9OTB27btC2LanZH/fqyn4Z4MZPB8Ka+yz+EWHlomlrVJH04ERRD82ahnIRMUTIRxwfEfrL9VqyirDWh2uf6aps1ugAscv2F7i0cO1TPb4D5pt7k9H8oJsPBjfD+vxDB1Ry7abPnakuRgAQ+OLGGeKetLjFUjWi7/7MbrjcueCRG/g08vDz5btmMmE3tiyeRkyp1/tg28l26P+Koogi3gTln+HS0k60im8pZjiy8uEB2B7eiix+s/Too4YRRrXR8B3N21Zq3ZwAaQWU7LszSka3ufN3pS7vXhzwXqG3DgSBZsV3EqSLJE2jEuQJDmqRGekukRlx5eWM4Ea1AgztaVQiwv6O2yAhj14DLupNk6oJQN9Z/V5EgkkmNmx8mejmu3+PbMqWjsl+2AoUZnaDr4WC6HAbCL9LbzaOCgLOCtNoiAGrKTpSWptydhOd3ZOsmo25vzFOmsF1lrDpM/R1yBJjyj2QKje2JbYppjn/pgh1nCQT1nsGDacwTEeUTiOWZSNALKfBaS0JLxnebIrHYfas4l8X5wOFQ9WP+pptcT6AZO3o78XJiDHiSFNuc4SsNU2HacH7T8duLoJ2RJEM1nnVqpDmk5Zx6uE0SFpnh0qnPmNMPtvuwU/QHt/6FNTHglHKp9sHZhWipiDvXS3k2l7XPCfGLrG+zAULaRTNWlJYQ8xYOSPazvOST8zGp6PPzXQVYxXVah/Fra3u9nD7d9pcatzsCqssbPHDZL+o4ZihpVT3YH3TsIlDtIgIjouMYg5NtOsHUoV5b2eyKHUw1fGMRtwwVPEn5Jr+YR9U4JcvIU8uEhBtdst+NHhKD4y/BPkwgutAJ9batN+t593NtQOACP5UsQjtNPwoOb2JsXT5s9qTouLLEnk3qIpoDGUTceSHCLLyyo9ByWXUyYbnv5U45WJUElm0OwLp7dKcIsthUt2bBCkxyYWPrb393jPc6Xhz64+jwIeBKFnhfI0bOCPZg1ZI8Yc7POlJP6Z2RxBvmdCzyClWPQWY7y/Zn109KDQ4JlrzOzUlh8Xlqf3/k4FiViJvFDglbHw/bv9/09DQ69DnVuPy/hUMWrmCb8vK5T3991DhE49/+7drS3dtZT5ORorDMQ8xwcZmTMnzeLTuc18u8GV6/7B7V4j4WHPmkAuv37s/x5EifC/vujXJUb3pbX/z9OUtOxMdTYiQAAAYVpQ0NQSUNDIHByb2ZpbGUAAHicfZE9SMNQFIVPU0UpVYdWEHHIUJ0siIo4ShWLYKG0FVp1MHnpHzRpSFJcHAXXgoM/i1UHF2ddHVwFQfAHxM3NSdFFSrwvKbSI8cLjfZx3z+G9+wChUWGq2TUBqJplpOIxMZtbFXte4UMAgwihX2KmnkgvZuBZX/fUTXUX5VnefX9Wn5I3GeATieeYbljEG8Qzm5bOeZ84zEqSQnxOPG7QBYkfuS67/Ma56LDAM8NGJjVPHCYWix0sdzArGSrxNHFEUTXKF7IuK5y3OKuVGmvdk78wmNdW0lynNYI4lpBAEiJk1FBGBRaitGukmEjReczDP+z4k+SSyVUGI8cCqlAhOX7wP/g9W7MwNekmBWNA94ttf4wCPbtAs27b38e23TwB/M/Aldb2VxvA7Cfp9bYWOQIGtoGL67Ym7wGXO8DQky4ZkiP5aQmFAvB+Rt+UA0K3QGDNnVvrHKcPQIZmtXwDHBwCY0XKXvd4d2/n3P7tac3vBy8ycozXcU5bAAAPnGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6aXB0Y0V4dD0iaHR0cDovL2lwdGMub3JnL3N0ZC9JcHRjNHhtcEV4dC8yMDA4LTAyLTI5LyIKICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIgogICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgIHhtbG5zOnBsdXM9Imh0dHA6Ly9ucy51c2VwbHVzLm9yZy9sZGYveG1wLzEuMC8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgeG1wTU06RG9jdW1lbnRJRD0iZ2ltcDpkb2NpZDpnaW1wOjlhMzNmMjJhLTgxMDktNGFiOS04NGFmLTVmYWYzOWYyYmIwYSIKICAgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDphMDU4ZTA1ZC0wMmI3LTQ1MTEtODJjNC04NWZiYzg2ZjIyZWUiCiAgIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo1NzM4MmU2NC0yZmQwLTQ0OTItYTZkOC0zM2VlNTI3N2UwZDYiCiAgIEdJTVA6QVBJPSIyLjAiCiAgIEdJTVA6UGxhdGZvcm09IldpbmRvd3MiCiAgIEdJTVA6VGltZVN0YW1wPSIxNjEwMzE4NzA5NTc4Nzc3IgogICBHSU1QOlZlcnNpb249IjIuMTAuMjIiCiAgIGRjOkZvcm1hdD0iaW1hZ2UvcG5nIgogICB0aWZmOk9yaWVudGF0aW9uPSIxIgogICB4bXA6Q3JlYXRvclRvb2w9IkdJTVAgMi4xMCI+CiAgIDxpcHRjRXh0OkxvY2F0aW9uQ3JlYXRlZD4KICAgIDxyZGY6QmFnLz4KICAgPC9pcHRjRXh0OkxvY2F0aW9uQ3JlYXRlZD4KICAgPGlwdGNFeHQ6TG9jYXRpb25TaG93bj4KICAgIDxyZGY6QmFnLz4KICAgPC9pcHRjRXh0OkxvY2F0aW9uU2hvd24+CiAgIDxpcHRjRXh0OkFydHdvcmtPck9iamVjdD4KICAgIDxyZGY6QmFnLz4KICAgPC9pcHRjRXh0OkFydHdvcmtPck9iamVjdD4KICAgPGlwdGNFeHQ6UmVnaXN0cnlJZD4KICAgIDxyZGY6QmFnLz4KICAgPC9pcHRjRXh0OlJlZ2lzdHJ5SWQ+CiAgIDx4bXBNTTpIaXN0b3J5PgogICAgPHJkZjpTZXE+CiAgICAgPHJkZjpsaQogICAgICBzdEV2dDphY3Rpb249InNhdmVkIgogICAgICBzdEV2dDpjaGFuZ2VkPSIvIgogICAgICBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjQ2ZWM2MGI3LTNkYjMtNGYyMS04OWE1LTU3MTk2ODViNDk3NCIKICAgICAgc3RFdnQ6c29mdHdhcmVBZ2VudD0iR2ltcCAyLjEwIChXaW5kb3dzKSIKICAgICAgc3RFdnQ6d2hlbj0iMjAyMS0wMS0xMFQyMzo0NTowOSIvPgogICAgPC9yZGY6U2VxPgogICA8L3htcE1NOkhpc3Rvcnk+CiAgIDxwbHVzOkltYWdlU3VwcGxpZXI+CiAgICA8cmRmOlNlcS8+CiAgIDwvcGx1czpJbWFnZVN1cHBsaWVyPgogICA8cGx1czpJbWFnZUNyZWF0b3I+CiAgICA8cmRmOlNlcS8+CiAgIDwvcGx1czpJbWFnZUNyZWF0b3I+CiAgIDxwbHVzOkNvcHlyaWdodE93bmVyPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6Q29weXJpZ2h0T3duZXI+CiAgIDxwbHVzOkxpY2Vuc29yPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6TGljZW5zb3I+CiAgPC9yZGY6RGVzY3JpcHRpb24+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgCjw/eHBhY2tldCBlbmQ9InciPz5S2H3wAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5QEKFi0Jmd3UzAAAIABJREFUeNrVnHeYVdXZ9u+1dj91+jDDFBhghqGJ0puOcBQLTUQ0ikFNbIkmlmhi8qaZN8YYY4kmltckRokFLFgAhQOMIIggvTswvffTz9l7r7W+P6YwQ7PEvHm/+Yvreq51OPt3nue572etvTfB1/zz+UoUAJLfXxr3+UoIAANAwu8vZV8iJvv9pbGvGJMBKKeLXVYA2Rw8XfGv/zjW/d0cp1v3JWIGAMvvL7W7Y6rfXxo9TUwCoPn9pVHy/yc8YdSUPCjKMoZMo9xeTASfyyR1G4R48ZzDH2xK3f9y0L9+05ngmX0A/UvwAID0gUL8/lLR/W8ZAPf7S3n3xWjdFyP+k/CMpw6pBmcXh3XvFTahl1PBMicbwPhsNz5vN7G+LQqbys2C0OWGFXtlfOkf93204tnI14B3plg/eD5fiUH6fOF4N7DTQThb7N8Kr3PMDc7dw32TBLBIshPzmaRkTHMrmJypYFi6ggEpThACWGYCgThwtMmEvzqCPRETXFIPAWJZdkft28Wrbitbt2kv+6YyrydGun+Z/zvwBEtsuPpVWbGiF1AiXZuQtcsEkD5dZ5iW68KwAQ6kOikEsyGrGsIJhtqmMNKSdKR6NAjOYFk2msPAvroY1tSEUCskWxC6WWbmy0Maj6w5/OvLGr8JeH5/qU18vhL6H4LXp69dQAIjrvUeHDF7SpzKCwln8yTBMiZ5DUzL1jE4mSAz2YAkSeCMoS2YQG1AYHdjHP6WCKJEhiAUs70U4zNkFOV64DFkMDMBGxKq2ixsr45gTXMUEaqGALzrjXW+ntt8fN2+386Pf114Pl+JRP6TorD5qpe4DVIi27HFtqzPE4SmTTGAGdkKCrM9SHPLYJYJIsloi3CUN0WxrTaC0iiFAMKKHS9lRFrDJeXPMud/Jdz0WrJ+iQS4LvMKjM9xYegABxwqwG0bUSajrDGGjyqD2BQBBKG1lNuvpUU7V+SU79rx2dM3ia8CD4BG/rfhDfvVB6721EGzOjTXXADzKDPTp3k0TB3owJB0BSkGgaJpsLlAXUsYx1oZtjZb2BUxQbjdwiRtNQFWjqjZvSVzy8MdG655XSeCh1xm5IbAncX/kJ6vGUZtcz2XlFxBqHCDk4uSKSbkezAoXYcKq/cHOVAbgb8mjIO2LADsMczIqy7GXmv64YiaLwMPQIz4fCX6vxte9YUPimPpBedTbl/TbTlSAeD6DIILCpOQ1t27eHdf64zYeHhzIw7bFCA0JHP2sjMRfCeiOkrN24eYfRU1/5FPk+rd6R1MUq7jt+S+4vOVGDsXPJ8fUB0fEsEM1Yo/l1CdkwFckC4s5eJMA2NznchLUSCDAZKK2vYEdlYGsLqFoVVQk3Dmd1vR1y3ZeDNy+6DImeD5/aWip4S/cXiOPx1UdcEvDuneKxihc4lg6WNUYK8lAUC1ZCc8kqwkXZlpYEKOioFJMnRdAwBEo3F8Vh3HujoT+2MWJGbvZ7L2EhH8tZIVS9sB0WtHGqf8KOVQ7tgmAlx34fIlb/dcaNKfPx8eIdI6JmsRCuHLaa9W67zZc5ikLCaCT8kijF6a48aoLB1ZbkBTFSQ4wbGGKD6tDmFVgIADHaoVe1sh9J9hTSud9eoS0hfeyTbmX4R3gRIYvdSxs/ji8QAWS3ZiAZOUjOk9liNDQUayA2sPBvF8eSAhgXzLlBQv5exWItjEUU6dXpJnoChdQrJHA6USLNtGTUsUB5o5Vjck0GwmLEGkjZxK/8jprFtd+OF9ic1XvSxbVAp6zchN41be+nJfRS1d/PJADrJethNxr524qOUHI6ouuOp78vHx1w+tT86dD+Bqysyxg1SFzM5xYESmigw3gabpCMRsHKwO4OMGC1uiABX8uG7Fl5uK8Q/r1tyj3Vlpk5NGm68ITyTWL36Falb4fELkaxOydhkBBkzWGGbkuVA4wIEUB4XgXaUJAIl4HCv3R/BKs9XptmKzxr1/1+HSK54dBpDvSnZiiSIryfMzdEzM0ZCTLEHXdQBAOBJDdQfDp3UmPmiJw+J2gEnqW4B4U2LW+wRYYt5e8M+TFTX70R2jm1wZqwWVIsmJ8MXnvnNba0/swjmLHLtm//eQkOa6kgh+DeF2YbFDJ7MG6hiTJSPVrQFUQkN7HPtrQ/iwiaPcEky2EzskQpfFZO0Z4vOVSF+1r0WGzHHvHbtoYlySFxLOFlDBsiZ4DMzothwDUk5YDs5sxLmMssY4orE4JhcmgQmKFz9twQdt8TpBlan2rbnVPl+Jo+ziR+SapOwFlLNbiGBTRzh0cmmegaIMCSndWck5Q0cogeNtHKU1MWwLxMCpDEHoP51m9HcT37ujYv2Ha/s1/g2LlxUQwddLzIoKSZlp3ZpX17+vXShVzfx1zvG0gt8CuE5iJu4c6saFI5IhOAezLciqhqaghb/taMGnUQFBJZFkxvL72pizwvt40Uu2RcgUxY5dY8n6FZzQARN14PxsBUXZHqR7ZHDLhCQrIJQiYdoob4xgbzPHe80JxOxEjFNFvbfQLU0b4kAoauLZnWFsjbD9Q+sPXJL38cPNPeXHtVT1o/lPFAiQ70rMXCJTOXVepo5JORpykyToRldWmokEWiMCBxtNfFgTQXnC4kxStwN4aXDTkfcK1t/fuK50l+3zlUiHLnlkZKMnexWAcG571eXD/D+rXr/4Za7YiWlU4Humos9Po8SYk04wYbAbA1MMQHBYloXWKPBJZRRv1IYRA20UVGogghdM/uSvueRs8Mbe81e9btDUGW26ex6AKyRmDhzn0jAjx4GhaTLSnBSq1lWatpkAkWQ0hxgO1EbwQU0Ex7jMAWxzm9HXFDP0arsrc4kk+GM/H2GQcwuS0RGx8fvNDThi0Y2CSpfNXL7E7j9mXWAc9/1OqUrJm0c5uxVdWUkvyTNQnCEhxa2BSl1ZmUhYqA8CO2tjWF0fRiehMUGkNQozXx51dH3pzidu7pSerymizNogiBTkVPorEXwp5faoqV4DM/MdGJJK4XaooJIE22b4vC6ETbU2VnfaUJh5SAj+GJP1Vwhnf5G4NWb294aOJz5fidEXXuv4O+x9BZMnUmZdRSCuZJI6cJxBMT2DYkSOGxleDehjOQCgLRDFsRYbm+sS+DhkgjK7gsnaP6kQy0pWLKkBSMLvL2UlV94ifzrzvseIatz5u/FJyEsiaA4LPLg9gCbLfm3S1mdv3vLS78KnHbMEVzYuXlYgCP2OxMzrQeW0KzJ0TMrtykqjOyttM4EYoyhvMbGlOoL17XHYktoEYLkrHtwaU523MCpdmEQEFmZKGDfYi+xkDdwyQSUJJic4VBvB6mMB7DBlDmBdciLy7ND9b6359PkHErMuvkjavOCZrbaslfFbcpcQAEh9dIdMJXVGwJF8hU3oAsJZ7nk6cEGuG4UDdKQaHIqiglDa29cYVVDZnMD2qgDeb+eIgwZlzt5zJoIvRyXZn7ijmJ1u723nghfMgKq/kg1r8c+npCM7Rcexhgh+/lkbolR9dEzV1vvTPv2LfrYZtXLmg3J5WsFcKvithNvTiwydXppnoDiDItWjg0oSBOewLQsBk+JwQxz+yiD2JIDpHhUlORoKM2R43Y5u6CaCCYF9dQm8WRlBXcKMMVl9jYA8UbLy+2XEDNh+f6nl85VItqdQ33TJL6oBPDFz+ZI/EPpczSUSM59jkpJ3rkPG9AwFwzMVZKc5QQmBbSZ6+5pt26htjeFQC8ea+jjqEgmbE2mToNI/coNN7w1bc1fcv35zbPAv15CUYL03ef/LodNtXI790cspB4ae//YIQzn/3klJ8GgCBxtt/HJ/SBhm5P7J7975hH/t2i8c8IWka6UL/6dQELJbYqYlUVmZk65jcq6GvOS+WWlCUIpQHPAapHcjQgigvi2MrZVxvNNsI8x5i2rF/seStaft2wY19FiVHngAtI8X/cNrUqnWY0ZvGL/ylteI6+mjP48pxoOXp2lYOs4LKjhkVe0tByorCMQ4jtRHsKEqjO0JCQA+d1ix18GsF8M/GFF+MiDPnw4tiWiuhwWhuTOXL5FOt320YfEyL+Fs0xQHRtw5fQAMTcJHB1vxRIXJBKHXzFx+3UqAfOGALz9X7SLcDhHgZltSw1TwWwi3LxhmaPSyPAPFGTLSPFpvVjLbApFVVDQnUHqsE+93MAgq1Sic/a6o+rM3MrY/1dpjkk+GByC2YfGy6ZRZmxxW7LzgD0bslk3FEBql+KgtgcKyAC4YkdoLrzMOvHuoA6tb4+CctTNJfYNAvDR9/SN7lLb9cb+/lHme3F+wVTbupkS6B/7B8PlKHB+rzhRBaMa0zX+RewTqFAhYEjh68cNXbqU5/tRdnQOvHevEjOHJCLOQ9EJ19B975jw1r/WuMeu/aHekqHavcSRnDASRojOXL1khZMe7H13xbO4RU9xUdrRzKSmTMuek6ZiSpyPDSVDVKbCmPIgdoRgHoRsFlXUi+ChvtH3/F8Hz+0uFY0F4hCkbZlj3HPP5Sii1CWXDJIF7ix14osLCnupIb9lajOCjtgQ4s3cIIuXzW3JuvXD59bt74Pl8JbIkqdPiin6HKUmunhnVpBIjgkMO1Z1147Lm/slHCDBvdUskuOpgBCAUs4vdWJhCHe2OlNel52uLv2h3JOrOtAWRoHLWtTtiR2P2rXlHZy5f8qshzWVDbSItfqc1sf6nn7Wymz4O4lcHQvHdwegrmh2fYN82yDcg2jFXZlZZqyv9rY2Ll43w+Ur0M8Hz+Up0LuvDOZXqp298LApApxIzQQXDuQVJ+FGhE4/sbUNFGwOhFJkeCfeNcgCyNoFT6SczL517iigEVWcCAAqayoyeAV9iFqHcRuWU28+2Zd41DSxfclgRuHZZk2WuP9QJcBuLJ2TgIq+cSpm5et/cP6efPMT3/cyqpGyLCA7NitK+MyoA+8gvZ4dnLl/y1oWvLZrHJfUhAURzgs1jp79x/Q2RO0fs8vlKpBHv35mgVL4UQCNl5od75v4l/0zwIBgzqTyEClGmNm/XAMSozrkqpK6eNyFPxZJ8N/6wN4Tathg4szEqPwk/G+mGxMyfbp7/1C0ni4KAEBIzEXCl9A74BrNUJqkghJxp17cfhOj3ClbJzP7es+UhvrvOgq5QLDnHifOc6qBWw/tWzuN7k08e4ns+MyvUJFFuI6y6rDOVn7Zxe0yATFRs89PCtfeX+zds7Qcodlt+a05HzQJBaKzd8Lyf/Oey3FPgAWz94ldtIniRbMfKQOSY318qaETRTUDANhNQFAWzRyVhZqqEJ7e3IWBSAMCYbBk/LPQQRuVH9WcqlvQTjHhY5VRGkyu9F2xEdSQAIP+TZ770lvmMt29+lUnKr35/OIwDVZ3wOFTcPikVw2QxrkVzvv7ZwhfYyfB8vhIppaNOY5IKceLHOqX8PniuypCYOZUT8aF/rf+02VW47oEaTuXZRHA9QuiarYtedPaF5/eXWlmdNQ7KrHxLNg72xCggAHZiBCOCY94oF4Z5DTy3oxOdwSgkWcb5RUm4LVeVGMQLGxcvO78HgqkaXBCKrEi7dCIrAYmZsJ2ZX/q8Yf3addH0ROi31Ir/7dEDUdR22khxSLhzrAseVb8oKOvPnH/VLY6T15UPHN31Q5pR7Uy9yx0PzeBEcjNJW3fa0uwGNHP5kqqMUNMCW1Yz4lR+r+hXH2T0xHy+EkplrYjJqiYIOdqzjhpmVCZU6WeSHYaO68YlQeImlu0Nw2QEQjDMLHTiWzluTQAr0h/fez4AK05lkwgOd6ChNysdZkTmVMKW8+/sudAvdd4w+p3vacPqD97RAfr+45+1o6kjivxMN3481guNmzdum/nAT0/uh3EqCcpMxKhknqF3GXHNNUtQqbm44cDBM8HrWTfyg/t2AeRyiVnDjw8Y/tqGxcsUn6+EAtAbXen5AEFOsKmq+9jXorasC0Fovx1hdMkZbp6YjoYEsGJXO0zTgqZrWHBOEuZ7hbvdkfTGhsXLhuYGGte7E6FFMW73nnQxWYcgEqZuee4Lt8VPju3/7fzYwHDr9dVxc+fze6MIxRiGpEq4b0QSbEn5qfps1W0QrBfC8Lp9uiASLFkTp8suIbtsk8olkuCbsjY/LJ0BHu27bubyJfsNbi/iVJ5EBH8tULTQAyDGCSmkzApkVG6r8/tLTZ+vhFKLSkwIBmafgNdloGWkuFXcMc6Dba0xrD0aBxcChJm4ZnwaZiVp6USwNYIl0jt/MPLNqgcv7+1PCSrZRHAogaovPFM4Xaxo9V0mJGXuzhg//tdtTTA5wfghHvxgsE6EsP+08epXZvVmlzONCXrCxpwMaOPCZ5OI4OfqZmTDWeDpJ8eC3x/ml7l9DWXW7J1jrnj6o6teorIdHw5Cjn329PdjPeuoxCxQzk6B17P3lu4kuG9iOl6vi2PjwTZQSYZDV7D0PC8m6hhUm5L/pvJcTWpfCF2faaNuwk1nhXc6UeiJmbfmNWSEmq4sDYuWN/eFYNkMMwp0XJ/rUQTIK2lP7J8BwK5IzrGI4NCt6GmzizKrhDJLiinGmi8Lryd2/htL1wtJvQGEXC0x6y+CyiM5lY/2XUd1bqtBKOiM2KfA491ZOShdx49H6Him0sSemjg4Z3DIHLdPycQojY4mzHzv+EUPqz0QHMzSmKQipnu/Frye2Kg19x4SVLrizeZ4dPWeVsiahjljkjA/Ca5O3bNiw+Jl+QPCLRLlNkKqyzwVwgWSwu1LmayWuWxW9VXgdcVIzL4191XDiv7QptJ3GZXHSYKX9V1HI4phVjCB+zc24LOqODhoP3g9WTkq140fFXvwuwMBHK0NQ1Y1JDtlfH+sAwM1dUpVcs7LxQ+uMwBoEcVYA+A3hWt/pn9deD2KOnP5ddvcidB3/94k7M2fh0GYhavPS8H5Xi2TCL5KshMD+9iYfqJgpY01TMU4HyDrx6/8jvbV4HXFJt3yB6dMpFpO5VoiBHUmwpV911EAKCIWbhruwQvH4nhqczOqm6OnlrQkYeIgA9/JVfDowRhq2xOwzQQGJBv40YQUpBA+tzJ18PNHLv1T3L419/DM5UseJSz6L8HrihFt0msjXwNwzxNHO7G/Lg6noeLGcV6MUXhhXdLAf3bZmJh2sqJ+cuG9AwQwxGNGN38BPN43JqgW27B4WYb2588f2HneVfvDsvbOBS6aB2YipLkCfddRw4xIuixj8lAvfj89CWmqwD07I1i1vxPBcKx/STMbF41Ow6WZGh7b1oyOmACVJOQmK7h/tBNE1ZfUu1N+P3POVY5/NfP6xj7cClHy7l0vUOCPDx+OoqwxCqfMccfkDOTLdCxlFuKUmCcrqk2ojzLLckVa1n8BPNPnK6FW+ljHR1e9NGHjor/+kzKrMl3VHrplkHvwM1OTccs4F7oyncLnK9F61lG723IIzuDVgCWTM/G787zYWh/Gb7d04nB9AoydKGmJElxebGBkkoZndgYRiJhgtoXiXC8eGOWGzMx7P778kTu/ANCXhtdbKvFW25bU+xOcv/roZ61oiQIZXhV3jXHAKVGYsl4y+XtPyn0BSXb8IhDyWe2PpzWfDZ7niX0p2xc8d/fHF967jRFaOt3g3/rlGK/+mC8Ll4zyIM0hoKgaQAg8VkwFIHqgS2Lej6ZlUD7rgjwNsqqBEIIkjWNavhsKB/54OIRwaxSD0pxwaFLvyDciy4n9dRHsqQ3j3DwvFIkgzRDIk2WyuYPPVC7/QW3+oZVHviSgs8LrKb9ZsV3UK7k2HEsvnFjRZA4em0qQlaKj2KWitCF+XntqPsvd/+p6/8ZPxLi7nvc0pg97nFP59VmxXR+dDI+58njpgj+PpnPuepBR6QVVNeZeN9DIuKFIx+zhHmSnOiDTrhZmQcHB+hg2N0ZAOXvt6NP3Huj5QYj8TMUDo1Xy0IOX5JzoeZLcfVjDUd8WxTtH49gWtPHdQQomDfVCV2UIztEeiuPJz8IocEm4+hwXNEUGoRLWHezEs+VBi0vKleyW3Pe+CXh9YxsXv5xEmbVpkksddce0dDgUgq2fd+CR4yYHyA0zly9ZseuK/xnXqRgfa5xdOO2Npdt7PrP45++m1mSPujIqqTdBiIkTdUYuLkhCcbYBQ2IAoZBkuevaWyPY38Twdl0Clm2hQwCCygtnLl/yTs8PIjkvu2OWouglU7J1yMLqB4/bFpI8DozLMZBHbLxSEcfBBhM5DgKXKuByGhiVpmL50Q5YMYHh2U4QwZHrJXCaVNodYvNSfDdty/p8deU3Bc/vLxWzYrsRGnLR6qNEXxhrT3iK0yUUDHAjxeJkZ1vssqbhcz4NKcZkIsTEUWWlD0BNCW2+5DfDlUtu/682b/YLsqwsXpih5NxUqJG5Y9KRk6JBEjZAKBiRUNYQw5t7W/GnCgs6A67MV3H1SA/erjGhCL48/9DbvUossQU/ntrBMWtbZQDpsoSsZB0UArz7MBkAuGViYKqBGbkudAQT+OPhAHSTIidZhVvhGJmi49myGLyWjVwvharrGJqhg7VF1N2mMrdi1KL3Z8V2tX0T8Hy+Ep05c/mhkXMyieBF5ZFE8WBNRX6ajjwvhRxl8q64dDkRfIhkJ440pg/rrMk990nKrEeKDWXK0mEe44bRHpybJSM9yQlKCWzLRGeUY1dNAs/u7sB7tSGMS3Pgu6O8uGiIjuwUFbKsYPmxMLyJ8Mqso6v2dH8XjdDnax8oJNZDVw7z4MXjcQzWgIWFOoZme0FI/5LuOemqCQi8ejCEplgcNxR7cU6+G583RPGLnW24f1QKJhS4YJsmLE7wj50BfNBh1mZ31l5ctO6nR/4VeMN+/WFmfUbhwpikXE+EmDxSscnsQV6cm+eEITNQQmFDwrOftKK0PQomqxwgdH4SMH2wCwUDnJCIgG2akFUNXABVzWFsqYzhnXaOIoXg0hwJYwYldd2gaVkAASRZQcJkWLSmFpIkL2S35r/drcRC1s2oZDgNTB7mxfAsA2sPdeD+PTFc08jhG6Ih2d3/QEbRNBRkAHe7ge2VCh4/HMXUugTmFRm4f1QKfn84jF9LDMU5LhiqhCXnJSGwpSlna3LuW/WLl13gw5Lgl4RHAWjlFz+cqEzKmaYujH+ngiqLnJLkujpNwfhsGQVZbsgShW2ZACjqAgw7qoLY3RnDIEPH3FyDjhggI82tQpJlCMFhmyYSQsGeigjWlgexK8YxJ92Bh87VkZtEoetdQtoXnhAClpkAThh2rUeJZS5r3TaGw6UKXDUhExPbElixtxU/2RrDjYVujMt3QBJ9JxMTuqqgpFjHiOwEVu5rwz07GG4e5MC3BlD84UAED7p05CYTaBLDbVMzEfukffj+SHzVcd/vZlf8ZEr7WdX20jlSxQU/HVyZknulAFkqMat4okfHzHwXCgdocEgciqoChCAUieNIQwIf1ZnYErIx2yNw33lpGJppQBJWP1GoaQ5jTyPDyoYAXMLCgnwHbs1zI9UlwzYTvS6kLzzL5jhaG8CmWhuUEHiseD8bQ+jztQ+MVvDQr0qS+00fnEjYWxPHi0dDyJIZrhmTiiGZBpjZdYLfNyupouJgbQz/ONAOKkmoZxRphOPH493ISnMCIGhsj+CR7QEcZ/TDQZ11CwvW/hgnw9tw1TJCuXWJxO2bLUWfnSVR5dJ0inH5TmSnOkDAYZsWZFVFY8DC7qog3qpNQJVkzMlWMSZLxsA0VzcEEyAUNijKGmLYcLwTGyIUMz0KZgyUMTzbAUNXIYQ4LbyoTXGgNopVxztRxmTMT9fwekMEDmYuCt9R9OYJG/Ns5QOjFfHQg5fk9mZXX0AdoTg+Kk/gpfoEFqUSzB7Rc0cph21bXZkAAts0EbOB0mNR/L06CnALo50G7pmaAqfMQSlFQ4DjwU/b0J6Ir8iIh6+tvvdce9Yll+l7L398WLvmvI4Ifj3lVvbFqU5Mz3NgUCqFQ5NBpa7yi0QTqGznKK2KYX17DOd7Fcwc7MawTB0KrF6za1sm2sIMBxotvFMdRcQyMT/HjXH5TmS4ANqdlSfDsy0LjQELu+pMvFUXQzqxMa/AizG5TijExtXrWkClfjYmLhvMUkOqA8EYg0Nip2RXiteBK851YGxWCCuPRnH/lk7cWKDjvFwVTofeC49KFG5VxpxRMkZlULx5NIHNYYYXPmnGrVPS4DJkZCdx3DPagV/tI1fVKkaL46kjuzcpxk2MSpMKFdBLsxWMzk9HmlsBs0xQSkElGS1BE7srAni/kaOTAwvSCZ6amtonK00oqgZbAOX1IWyujOK9ToFzdYJrBykYlZ8Oly51Zxc5BR7jwLGGIDaWR/FBCChxSbh7uI7iPC9UmYLZFhIWAEmGLDjpgef3l3IiPV/7gAAeyoKF64e6MWGIFwoVvffE9c1KTij2VIbx0uFOpBsarhnhxqAUCZLUdaE9Kq1oKiwGfFrWgRfLY5iYpGPphCRQbkNRVRyoi+GXu9ogUYoFAwyMH6gjN4nA0PXeDLI5QVW7jS2VEaxqiWCc28DFgxwYli7Dpcv9RCHKZByqj2FNeRCHEgILBzgwKc/AQC+Bpp1eFGwzgTiXcbA+jlXHg/jc5Lgq24mJeQYGuAGlp6Rtq4uBkLDowyakJELfOu/d7y3vfvyj28ZQ+6FFw9x46XgcGRJw5VANI/K93ffG9C9p27YQ5zI2Hg3hpaoQ5mXouHxUMlIctBce0AWBUorWiMDvP2nDxCRg0YRMSN2+q6HThtelwa11lU6PKLQFYthXF8cHdRaqTYYFaQSTCpKQm6YB9glRYJyjqjGMnQ02VjaZyKIMc/MdGJPnRpJDOqMocCFQ3xLG7gYbb9abSIGNBYMcGJPvhtc4aV03vL42RqbyQvu2XhuDXhszaagXxdkOfHSkA78+GIevnuGyoRoGphr94CmqCgUElxU7cO5AFSsPR3HPphYszZMxtSgVSh94VJKR7uL4wTkOPLg7iuTDQVw4zIAkUeSmO7ozyAJRVJS3JLA7VK6rAAAPDklEQVStIoCVLTYKDQVz83UMz5CQ5nUAfUTB5BRHKsNYXx7AlhjF7GQVPxlhYFiWAV07sygIKuPzhihKj3ViVZCgxK3griIdRdnG6cWkD7y4ybCnohNEUsFP2Bj4/aUJmcsq6bExDplj7nkZmBiw8P7+NvxwexzfzrFRMswJp8JPCIbV1fNy0xy4fYqGPZUB/PO4hY+aWnF1oYbCLGe/ks7PdOM7Qwj+cKQTLkVgSmEShODoCMbweQvHh9UB7IskMD9dw2/Hp2JQmgows58oNAdt7Guw8HZNDGAmrsj3YEmeE2kOAXIGUWCWhXCC40iThVUV7TgSTeCqgS48PcKJAR4KelJJnwwvZBLsORbA2+UBdKDrRgF3l42B31+a8PlKiGxS2RaC9+t5qYbADZPTcH6rjTcOBfD+xiBuKEzGhAIFkrB6s0twDsFsjB+ajOJcjg0H2/GLvWHMqWe4fLgLSbroLU2N2KCE8kfK4vA1tVAHtbE+QDBAJrg0S8b3x6cjzaP39jVF1WBy4Hh9EBsrYlgbEpjioLh5qIqR+ekw1B5ROBUeCEFDexQ7KqN4u4nBSwXmDZRwz+AsJDmVftl1OiVu6DTxWZ2FFfUxDJUZritKxrABBr69vhFMMNYDD4AhU26DcnGSYHQBGppBcZfXiwMNNl4ui+DDyiAWD/dgZK77hGB0Z6VGGS4/JxnjBnO8fTCAezY14sahXkwdqkCGBQGCCU6VfHdcUsfB2qD4vENJBIWd/fRkDzwOpZ8ohG0J+yvDWFUZRJ0FLMh24Mniruau9hOF/opKZBXHmuL46HgQq9otTPfquHukEwWpEpwO/ZTS7AuPC6C8IYQNxyNYHQRmuiX8dISOolwvFIkiFjMBCERVJ+v7LKFs2KYaUZ2IJBg0ynrh9QAyDA0TCnQUZkj4pDyGBw9EcGF1HJcN1ZCf6e4tsZ6szEri+O54N2Y0O/DSkTA2VIdxTbEHoDIoTJLmVVIu8KZiWFsC725phqpIkGQZNuOoaAhhRwPDGy0miiWG+YOdGJXrhlunZxCFLniBUAxHWxjWVAVxMGJi0QANT05Jx8AUBdwyT9vXeuAxouBAdRQfHAtiZ5RhUZYTTxWfqsRCCEBSIHXZmN4HMeWoapjHbIEH1jdgSaEb5w72gPSxIz09z20ouGSMgXPzElhzsB1372S4PpvhggIdyS6l1+zapgVN0zAmj+AXqRK2HI/g1wciSCECQxV24rF3wQFCELUIDjWEsPZ4ADtMCfNTVfx2tAMFmQY0VTmjKFBZQXPAxI7yTrzRyOGVCC7PknHn+DSkevSzioIQAm2BKA402ni7OoC4ZWNhno5bBnuR4jyNEgtAUhRACLgTYbXPU6yqLEAwjNhYMMSF54/FMaQmjiuG6ijM8fSzIz1ZmWIAS6cOwIzmBFYcaMeq+iiWDvVgwmAHZGH39jzbMuHSZFw6JhVjcxNYsacVcaGeAEgoKJVx1+ZWpBIb8wd5cGuuA0n62UWBCaC6jWFTRQDvt0Qw1evA3SM9GJIuQVfoWUWBygoaO018cqwDbzVz5KkSFuepGJObBLdTO/26bngdQROEmQiqzkQPPABE1q0odTgMTC9KwqgcCxsOteOBfTHMq7Mxe5iOAUlaf5PcDSg/heLuGek4UG9i2dEQPqjoxLdGpmDEQBXCNkEIBe3ua2kOgen5Hvir4v3evSCEwN1FGkYNyoSu0DOKAiEEoUgCB+tiWFtrYW/MxqJUgsenZiI3VYdgZxYFZltgHKjpYPioIoD3myO4INmBB85xoyBNhiKRM64TAmiJCHxa0YHltSFQSW3ihJT1wPP7SxOyoCoVRIIQHE6Z44rxGZjUaeHd/W344Sdx3JjvwvShDhiU98suSilkScZ5+RKGphJsr7bwu/0hTCkPYu5wDwZlOvspKiE2wO2++CCYiZF5GX3gnTpmtYRs7KoM4I2aBByyjPm5Om4bYCA92XlWUSCEIBo3cbguinU1Fj6N2FiUCjw+NRN5qTr4WaBbloXypjg+rjHxfrsJYieOcKo8ISh5eebrS+weeD5fCZETUreN6ZNdmS7g5qnpKGm28PrhIFZVB/Dt4ck4b5AKwvuUdHfP87oMXDTSgTFZCtaVhXH3ZyFcOyCBkgIN6UldRrjnC/f7674J82R4iXjXE+mbK9vxXksMU1wS7hidhKIBXccOZxMFWdUQinPsrQri7YoogpBxZY6GpaON7vcr9C/NvutMBhypDWL18TC2xSknEB9746GnU9vKV5b9eo7VN/P62BgGm3EQWT1FUYdnUdyX5MG+eht/K4tiXUUIVw53o2iguxdeD3RmmUjzqLhuUgamNsXw5oF23PeJiRsKGMbnaeCcAFTuQ4/0/n+K1qXE0QTD/qpOfFjDsDfOsSCF4I8Tk5Cf4QLB2UXBNhNoi1HsOBLAipowchSKq4Z5MTpHh0bs0/a1nnUxLmP/sTBWlodwLMFMLilvyII/ef5bNx8AS8T79ry+8ADEZacVCx6mLjyztQ0LinRkp+j9sstp6JgylGB4hozNx6P42d4ILq9JYPYQDQPTXf3GrB4vl5tEcHdJFvbXxrDsUCc+qAqj0KMBQvTPQGaBEIL2KLCruhNvVQahyioW5DlwW5aCVLd8VlGQZAWMcxyvD+HjGgvvttuY4RC4f4wHRQPdvceSZ4LX0BbB7joLy+tNBG2rk3Lrr1w2nrpw5Z3VxOzo+84IFQA93fskpIJJC3a2udPeq47Gz1lfb+Z4GEG2VwZh/RXVUGUMH+jCjHQFRxvC+EuFDSPOkOkQ0JT+uyNdB/AUGU6C6bkOaKBY2RDHAGpjWoGna0yKWlhdG0MkIPDk5yHIiQSuLk7GteckYUiaBId2dkW1IeNQbRTLdrbixTobo1wKvjPcwMXDPchKcYCS08MjsoLqtjje39eKx46Z2BFkFcS2fp/bcuw7LT+a8Pas2K4gYfEvBc/nK1F7ntbUAyOuITtHzfk2EfyhETJLuX50KoZnO8C6FfUEIAuSoqKsKY7XD3Sg0RK4vtCL8/INUG6d5NdO9LVPjrTh4waO+2dmAADq2xK4fVsHrkxTMCVHRkGWBxI9uygw20Io1r1RWhFFq5nA4nwPxuc7keLAWddZTKC81Ya/PIKN7VEhJHW7IOTxc45vWZW68xnr5NL8MvAAUKlnO/3jV59OzIrt2i0yRy8v09NT/U3mKLMtQnKTNLgcan9FpQTJusCUPCcyVRnLjoWxqyqIbJeOVLcKbp+qqO1RgvIAw7TBzq4MjDG8VxPDf01wYUCqC/QL4DV2RLHxaBh/PBxFfcDGvBwJN41Lx8iBDugSP+O6cDSBXZUR/O1AFK/UxlhtOLpKUOkWAfqLmSuu/9zR8Bn/uvD8/tK4fPJdSwUbH6ws95den/zUkVdWtuqPbdzaOfymAgPjcrp3oPv0PF2TMalARlE6xdZKE7/YG4SvPIjLiz3ISXOcdDGn2hiws49ZVFFR2Wpi87EA3m01McWr4b6RbgxJo3AYZ59tO6MMOyuCeKMqigbIYQqxbGCw+blh636yb8OH684oCl8Fns9XQnpK+LQ33my4ehmlnN9DmPXTsS7Dec1INwpSpTNOCnWtEXz4eRjvdwgsHahhxmAVyR4HCCHYdawT/qoY7p+V1V3Ccdz6SQdWXJwJhbJ+EKKxOI63cqyrjOGTQBzz0lXMGOJFXuppZts+omCZCTSHgW1VMayoj8Dkop5Lyl8ctvk/k9+6MQyQsyrqV4UHwJDPessXllAAT3y86MVXdsb5H3Zvb73yinSNXD46BSnOU8esAUkqbpySiemN0a4xryGBpUM5zs1Rwbg4g42xoHQLRjhuY3d5J96rZWhkAldmyrhudAoGJDvOOmbZjKOsLojSGgsfdDJI3D4o2YnHuer458zlSxJdEMjXAvRFMfls98v1xOK3DaqcOffq63fM/s3f32xXHl23qaX4xjwZU4tSoJzmvGFwCsV9F2ZjT3UULx8J4MMKgqHe09kYG4QA7TFgR2Un3qwKIkXVMH+wA6MGKHAb0pmVWAAWJOyrCmN1WSe2xyUOQtZ7rPhTI7e/6N/yj/+OfROA+rz66rQx+Yvg9dwDglgTD91RuDr3sd0bO1THvU9WOe7/qKnNvXiYjqIsA/JpLnRcnobhmWnYUhHHi1VRjNf678ZQAryyN4zV7Sam6hw/HJOKomwHZNhnVdRAlOFAg4W3K6OoisfjXFJeAyFPzFxx42EI6xspzS/7QrZeG3NWeH1O4ntiGxYvy1GY+SgjdNHlaQadU+xCqiHOKApbDrdha+OpNmZ+iozpuQqGZLshUfoFShzDtvIwVjQxRIRo08zY3xnEk4k7imq/yb72Vd5mJ38deH5/qfBhSTNXvd8uXfDnP7/fZj7u39R43tLBXpxfqJzWVjg0DSD9d2O4ELhqlAMul/GFSrzpWADvtSZgUaWMCvHUiJrdyzO3/bHF7/+I/6fg9diYrw6vO7Zh9Tumzwx8HB7ku3DHhG9/69nq+G/W1kbSryt0Yuxgb/8Mol/CxvRR1Fi3Eq+tbMfmQFxQwbcwSX0sJ9i8qvCDe+Sui/nPwvP5Soj8deH1jbkq/SH2wn89N/jhre9WezJ/9psj8VtK6ixl3lANBVluENL1ngJAnH43pg+8SIJhT3kH3q1hOGIJS+H2+65E6PHAD0dv/gYBfVV4iTPF5H8VXr+Y/6ftFf7SO/Rnq57f0hF/YtNu+8JrGxlmDjW6bYxyqo2xLSgKRWeCYOfRTiyvDKIVSlAQ8veMWODZUavuqvCvXZf431DUr/MqQPkbg3firJTijaXH2sbdftHeIdMWvFIbfvTDhsig0W791AzkNqrbEtjXxPBmUxzMTtRwSX1KEPLCzNevjYDQ/1hpftkXskkFBYPsrwCobyx+Wnjdsa0vP8pnxXaVq0nD/lbtzY5UJNjEiGlKg1QpoCnUrmyOmFvbTM3fzHEwYu92JsI/90Rabwn9cPSmWbFdHIT8n4fX18Z8+ezqc2fSSbHTXUw3hAvJ5xf9d1FdUu7vBaGXUG6rEEJwSdkiCf7raWt/s00OlEW/6THrf+PluuTfD69/bNAj27MCmnNIwPB2nnt0w6HkvX/Tv8y6/4vw/P7SGPH5StT/LXj/JkDqNykKX/VVgP8PydtDVp+FBKoAAAAASUVORK5CYII='>
<br />
-->
</body>
</html>
)rawliteral";

// Replaces placeholder with button section in your web page
String processor(const String& var){
  // //Serial.println(var);
  if(var == "BUTTONPLACEHOLDER"){
    String buttons ="";
    for(int i=1; i<=NUM_RELAYS; i++){
      String relayStateValue = relayState(i);
      buttons+= "<h4>"+devices[i-1]+" - Output #" + String(i) + " - GPIO " + relayGPIOs[i-1] +" - "+devices[i-1] +  "</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"" + String(i) + "\" "+ relayStateValue +"><span class=\"slider\"></span></label>";
    }
    return buttons;
  }
   //----
if (var == "TEMPERATURE"){
    return String(HTUtemp,1);
  }
  if (var == "HUMIDITY"){
    return String(HTUhumid,0);
  }

if (var=="SINK"){
  return SinkString;
  }
  
  if (var == "MOISTURE"){
    return getMoisture();
    }

    if (var=="VCC"){
      vcc = ESP.getVcc();
      dvcc = 1.1*(float)vcc / 1024;
      return String(dvcc, 3);
      }
    
  if (var=="TEMPERATUREVIJVER"){
    return getProbe1();
  }
  
  if (var=="TEMPERATUREGROND"){
  return getProbe2();
  }

if (var=="TEMPERATURECOMPOST"){
  return getProbe3();
  }

if (var=="TEMPERATUREBAK"){
  return getProbe4();
  }

  if (var=="TEMPERATUREOUTSIDE"){
  return getProbe5();
  }



  
if (var=="TEMPERATUREINTERN"){
  return getProbe6();
  }

   if (var=="TEMPERATURERESERVOIR"){
  return getProbe7();
  }
  
  if (var == "LIGHT"){
    return getLight();
  }

if (var== "TURBIDITY"){
  return getTurbidity();
  }

  if (var=="LEAF"){
    return getLeaf();
    }

   if (var=="SOLAR"){
    return getSolar();
    }  

    if (var=="RAIN"){
      return getRain();}
  
  if (var=="KLEP"){
    return getKlep();}

    if (var=="TIJD"){
      return nu;
    }
//----
  return String();

}

String relayState(int numRelay){
  if(RELAY_NO){
    if(digitalRead(relayGPIOs[numRelay-1])){
      return "";
    }
    else {
      return "checked";
    }

  }
  else {
    if(digitalRead(relayGPIOs[numRelay-1])){
      return "checked";
    }
    else {
      return "";
    }
  }
  return "";
}

void setup(){
  //SPIFFS.begin();
  Serial.begin(115200);
//sensors.requestTemperatures();
//temperature_koudebak = sensors.getTempC(ProbeBinnen);
// //Serial.print("---=> ");
// //Serial.println(temperature_koudebak);
 //for SNTP
  while (!NTPch.setSNTPtime());
  //NTPch.setSNTPtime();
  //---------
  
  Wire.begin();//(D2, D1);
  myHumidity.begin();
lightMeter.begin(BH1750_CONTINUOUS_HIGH_RES_MODE);
pcf.begin();
  pinMode(ONE_WIRE_BUS,INPUT_PULLUP);
  //pinMode(A0,INPUT_PULLUP);
  pinMode(SINK_FAN,OUTPUT);
  digitalWrite(SINK_FAN,HIGH);///ogekeerde logica
  tcs.begin(); //for color
  // Serial port for debugging purposes
  Serial.begin(115200);
//pinMode(1,FUNCTION_3);
//pinMode(3,FUNCTION_3);
yield();

// //Serial.print("=======>");
yield();
HTUtemp=myHumidity.readTemperature();
HTUhumid=myHumidity.readHumidity();
  // Set all relays to off when the program starts - if set to Normally Open (NO), the relay is off when you set the relay to HIGH
  for(int i=1; i<=NUM_RELAYS; i++){
    pinMode(relayGPIOs[i-1], OUTPUT);//myHumidity.readTemperature();//false paste?
    if(RELAY_NO){
      digitalWrite(relayGPIOs[i-1], HIGH);
    }
    else{
      digitalWrite(relayGPIOs[i-1], LOW);
    }
  }
 // setupOTA();
  //---setup AP
//begin Acces Point
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAP(AP_SSID, AP_PASS); //this makes your Acces point connection
  
  //------------
  // Connect to Wi-Fi
  WiFi.hostname("Coldframe");
  WiFi.config(ip, dns, gateway, subnet);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    yield();
    // //Serial.println("Connecting to WiFi..");
  }
// //Serial.print("\nConnected to " + String(ssid) + " with IP Address: ");
  // Print ESP8266 Local IP Address
  // //Serial.println(WiFi.localIP());
  //---for reconnect (nieuw---
  //http://community.heltec.cn/t/solved-wifi-reconnect/1396/3
   botclient.setInsecure();
  WiFi.setAutoReconnect(true);
  WiFi.persistent(true);
//----------------------
temperature_koudebak = sensors.getTempC(ProbeBinnen);
  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });
/*
   server.on("/fan", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/computer-fan.png", "image/png");
  });
  */
// <!-- begin nieuwe code -->
 // Handle Web Server Events
events.onConnect([](AsyncEventSourceClient *client){
  if(client->lastId()){
    // //Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
  }
  // send event with message "hello!", id current millis
  // and set reconnect delay to 1 secondtemperature_koudebak = sensors.getTempC(ProbeBinnen);
  client->send("hello!", NULL, millis(), 10000);
});
server.addHandler(&events);
 // <!-- einde nieuwe code -->

  // Send a GET request to <ESP_IP>/updater?relay=<inputMessage>&state=<inputMessage2>
  server.on("/updater", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage;
    String inputParam;
    String inputMessage2;
    String inputParam2;
    // GET input1 value on <ESP_IP>/updater?relay=<inputMessage>
    if (request->hasParam(PARAM_INPUT_1) & request->hasParam(PARAM_INPUT_2)) {
      inputMessage = request->getParam(PARAM_INPUT_1)->value();
      inputParam = PARAM_INPUT_1;
      inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
      inputParam2 = PARAM_INPUT_2;
      if(RELAY_NO){
        // //Serial.print("NO ");myHumidity.readTemperature();
        digitalWrite(relayGPIOs[inputMessage.toInt()-1], !inputMessage2.toInt());
      }
      else{
        // //Serial.print("NC ");
        digitalWrite(relayGPIOs[inputMessage.toInt()-1], inputMessage2.toInt());
      }
    }
    else {
      inputMessage = "No message sent";
      inputParam = "none";
    }temperature_koudebak = sensors.getTempC(ProbeBinnen);
    // //Serial.println(inputMessage + inputMessage2);
    request->send(200, "text/plain", "OK");
  });
  // Start server
  AsyncElegantOTA.begin(&server);   
  server.begin();
  // //Serial.println(sensors.getDeviceCount());
  Telegram("Coldframe Setup Klaar");
  //bot.sendMessage(chat_id, "Alls klaar", "");
}


  String getHumidity() {
  return String(HTUhumid,0);
}

String getMoisture(){
  //soilMoisture=analogRead(A0)/10.23;
  soilMoisture=(pcf.analogRead(0)/255)*100;
  return String(soilMoisture);}

String getProbe1(){
//sensors.requestTemperatures(); // Send the command to get temperatures
  temperature_vijver = sensors.getTempC(Probe01);// soil?
  return String(temperature_vijver,1);
}

String getProbe2(){
//sensors.requestTemperatures(); // Send the command to get temperatures
  temperature_grond = sensors.getTempC(ProbeSoil);// soil?
  return String(temperature_grond,1);
}

String getProbe3(){
//sensors.requestTemperatures(); // Send the command to get temperatures
  temperature_compost = sensors.getTempC(ProbeCompost);// soil?
  return String(temperature_compost,1);
}

String getProbe4(){
//sensors.requestTemperatures(); // Send the command to get temperatures
  temperature_koudebak = sensors.getTempC(ProbeBinnen);// soil?
  return String(temperature_koudebak,1);
}

String getProbe5(){
//sensors.requestTemperatures(); // Send the command to get temperatures
  temperature_lokatie = sensors.getTempC(ProbeBuiten);// soil?
  return String(temperature_lokatie,1);
}

String getProbe6(){
//sensors.requestTemperatures(); // Send the command to get temperatures
  temperature_internal =sensors.getTempC(ProbeDevice);// soil?
  return String(temperature_internal,1);
}

String getProbe7(){
//sensors.requestTemperatures(); // Send the command to get temperatures
  temperature_reservoir =sensors.getTempC(ProbeReservoir);// soil?
  return String(temperature_reservoir,1);
}

String getLight() {
licht = lightMeter.readLightLevel();
  // //Serial.println(licht);
  return String(licht);}

String getTurbidity(){
 turbidity=pcf.analogRead(0)/2.55;
  return String(turbidity,0);
  }

String getLeaf(){
  leaf=pcf.analogRead(1)/2.55;
  return String(leaf);
  }  

String getSolar(){
  solar=pcf.analogRead(2);
  solar=(solar/255)*6;
  return String(solar,1);
  }

 String getRain(){
 rain=pcf.analogRead(3)/10; 
 return String(rain);}

String getKlep() {
  boolean klepState=digitalRead(klepPin);
  String klepresult =(klepstate == 1) ? "OPEN" : "CLOSED";
  return klepresult;
  }  
  
void loop() {
//ArduinoOTA.handle();//ota loop
AsyncElegantOTA.loop();
if ((millis() - lastTime) > timerDelay) {
getSensors();
sendEvents();

/*
// //Serial.print("HTU temperatuur ");
// //Serial.println(HTUtemp,0);
// //Serial.print("HTU Humidity ");
// //Serial.println(HTUhumid);
Serial.print("SoilMoisture ");
Serial.println(soilMoisture);
Serial.print("Interne temperatuur ");
Serial.println(temperature_internal,1);
Serial.print("Deksel ");
Serial.println(klepstring);
Serial.println();
*/
//---
gscounter = gscounter + 1;
  if (gscounter == 2) {
    //sendData(temperature_internal, soilMoisture, licht);
    gscounter = 0;
  }
//----
lastTime = millis();
  }

  if ((millis()-lastmsql)>msqlDelay){
    mysql();
   thingspeak();
   lastmsql=millis();   
  }
  
  //if (HTUtemp > 25){digitalWrite(FAN_PIN, HIGH);}
 // if (HTUtemp < 24){digitalWrite(FAN_PIN, LOW);}
  //Serial.println(analogRead(A0));

  tijd();
  sinking();
  //check_and_reconnect();
  if (actualHour==6 && midnightFlag==0){
    midnightString="Coldframe\nMin temp: "+String(mintemp,1)+" at "+mintime+"\nMax temp: "+String(maxtemp,1)+" at "+maxtime;
    midnightFlag=1;
    Telegram(midnightString);
    mintemp=myHumidity.readTemperature();
    maxtemp=mintemp;
    outsidemin=temperature_lokatie;
    outsidemax=outsidemin;
    }
    if(actualHour==7 && midnightFlag==1){
      midnightFlag=0;}
}

void getSensors()
{
  Serial.println("get sensors");
  HTUhumid=myHumidity.readHumidity();//am2320.readHumidity();
HTUtemp=myHumidity.readTemperature();//am2320.readTemperature();
if (OnChipHeatFlag==1){
  formattemp="<span style='color:red'>"+String(HTUtemp,1)+"</span>";
    }
  if (OnChipHeatFlag==0)
    {
      formattemp=String(HTUtemp,1);
  }
htuhumid=HTUtemp+0.5; //integer conversion +rounding correction
soilMoisture=(pcf.analogRead(0)/255)*100;// pas op. eerste weggooien?
vcc = ESP.getVcc();
dvcc =1.1*(float)vcc / 1024;
      //return String(dvcc, 3)
sensors.requestTemperatures(); // Send the command to get temperatures
temperature_vijver=(sensors.getTempC(Probe01));
temperature_grond=(sensors.getTempC(ProbeSoil));
temperature_compost=(sensors.getTempC(ProbeCompost));
sensors.requestTemperatures();
temperature_koudebak=(sensors.getTempC(ProbeBinnen));
temperature_lokatie=(sensors.getTempC(ProbeBuiten));
temperature_internal =(sensors.getTempC(ProbeDevice));
temperature_reservoir=(sensors.getTempC(ProbeReservoir));
temperature_compost=(sensors.getTempC(ProbeCompost));
lightMeter.begin(BH1750_ONE_TIME_LOW_RES_MODE);
licht = lightMeter.readLightLevel();
leaf=pcf.analogRead(1)*0.3921568627;
solar=(pcf.analogRead(2)/255)*6;
rain=pcf.analogRead(3)/10;
klepstring=getKlep();
kleur();
if (temperature_lokatie <=0.0 && freezeFlag==1){
    Telegram("Het vriest\nColdframetemperatuur= "+String(HTUtemp,1)+"°C");
    //bot.sendMessage(chat_id, "het vriest", "HTML");
    freezeFlag=0;
    }
    if (temperature_lokatie >0){
      freezeFlag=1;
      
      }

 Serial.print("Baktemperatuur ");     
Serial.println(HTUtemp);
  if (HTUtemp<mintemp){
    mintemp=HTUtemp;
    Serial.println(mintemp);
    mintime=String(actualHour)+":"+Digit(actualMinute);
    Serial.print("mintime= ");
    Serial.println(mintime);
    }
   if (HTUtemp > maxtemp){
    maxtemp=HTUtemp;
    maxtime=String(actualHour)+":"+Digit(actualMinute);
    Serial.println(maxtemp);
    }
  if (HTUtemp>=25.0 && warningFlag==1){
    Telegram("Coldframe is nu "+String(HTUtemp,1)+"°C");
    warningFlag=0;
  }  

if (HTUtemp <25.0){
  warningFlag=1;
}


if ((HTUhumid>=95 && (actualHour>=22  || (actualHour <6 && actualMinute>0 ))) && OnChipHeatFlag==0)
     {
      WriteUserRegister(0b00000110);
      OnChipHeatFlag=1;
      Serial.println("heatflag was 0 is nu 1");
      Serial.print("uur ");
      Serial.println(actualHour);
      Serial.println("_______");
      }
  if ((HTUhumid<95 || (actualHour >=6 && actualHour <22) )  &&  OnChipHeatFlag==1){
      WriteUserRegister(0b00000010);
      OnChipHeatFlag=0;
      Serial.println("heatflag was 1 is nu 0");
      Serial.print("uur ");
      Serial.println(actualHour);}
  }

void sendEvents(){
  
events.send(String(licht).c_str(),"lightnew",millis());
//events.send(String(HTUtemp,1).c_str(),"temperature",millis());
events.send(formattemp.c_str(),"temperature",millis());
events.send(String(HTUhumid,0).c_str(),"humidity",millis());

events.send(String(maxtemp,1).c_str(),"max",millis());
events.send(String(mintemp,1).c_str(),"min",millis());
//events.send(String(mintemp).c_str(),"min",millis());

events.send(String(soilMoisture).c_str(),"moisture",millis());
events.send(String(dvcc, 3).c_str(),"vcc",millis());
events.send(String(temperature_grond,1).c_str(),"temperature2",millis());
events.send(String(temperature_compost,1).c_str(),"temperature3",millis());
events.send(String(temperature_koudebak,1).c_str(),"temperature4",millis());
events.send(String(temperature_lokatie,1).c_str(),"temperature5",millis());
delay(50);//was100
events.send(String(temperature_reservoir,1).c_str(),"temperature7",millis());
delay(50);//was 100
events.send(String(temperature_internal,1).c_str(),"temperature6",millis());
delay(50);//was 150
events.send(String(leaf).c_str(),"leaf",millis());
events.send(String(solar,1).c_str(),"solar",millis());
events.send(String(rain).c_str(),"rain",millis());
events.send(String(klepstring).c_str(),"deksel",millis());
events.send(String(SinkString).c_str(),"sink",millis());
delay(100);
events.send(String(nu).c_str(),"tijdnu",millis());
delay(50);
  }  

int ReadSensor(){
  int i;
  int sval = 0;
  for (i = 0; i < 20; i++){
    sval = sval + analogRead(A0);    // sensor on analog pin 0
  }
  sval = sval / 20;    // average
  return sval;
}

int Smooth() {
  // subtract the last reading:
  total = total - readings[readIndex];
  // read from the sensor:
  readings[readIndex] = ReadSensor();
  // add the reading to the total:
  total = total + readings[readIndex];
  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
    flag=1;
  }

  // calculate the average:
  if (flag==0){
  average = total / readIndex;//numReadings;
  }else{
    average=total/numReadings;
  }
  // send it to the computer as ASCII digits
  //Serial.print(readIndex);
  //Serial.print(" - ");
  //Serial.println(average);
  return average;
}

void check_and_reconnect()
{
  unsigned long currentMillis = millis();
  // if WiFi is down, try reconnecting every CHECK_WIFI_TIME seconds
  if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >=interval)) {
    // //Serial.print(millis());
    // //Serial.println("Reconnecting to WiFi...");
    WiFi.disconnect();
    WiFi.reconnect();
    previousMillis = currentMillis;
  }
}

void sinking()
{ 
  //will sink heat if temp >20 and sink < temp 
  //will withdraw heat if temp <20 and sink > temp
  if (HTUtemp >=20 && temperature_grond<HTUtemp){
    digitalWrite(SINK_FAN,LOW);//REVERSE LOGIC
    SinkString="<span style='color:blue'>Sinking heat</span>";
    }
  else if (HTUtemp <15 && temperature_grond>HTUtemp){
    digitalWrite(SINK_FAN,LOW);//REVERSE LOGIC
    SinkString="<span style='color:red'>Pulling heat</span>";
    }else{
     digitalWrite(SINK_FAN,HIGH);//fan OFF, REVERSE LOGIC
     SinkString="Off";
     }
    
  }

